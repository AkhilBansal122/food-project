@include ('layout.header')

<!-- Start Hero -->
<div id="home">
  <div class="cs-height_80 cs-height_lg_80"></div>
  <section class="cs-hero cs-style1 cs-bg" data-src="{{asset('public/assets/img/hero_bg.svg')}}">
    <div class="container">
      <div class="cs-hero_img">
        <div class="cs-hero_img_bg cs-bg" data-src="{{asset('public/assets/img/hero_img_bg.png')}}"></div>
        <img src="{{asset('public/assets/img/hero_img.png')}}" alt="Hero Image" class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.4s">
      </div>
      <div class="cs-hero_text">
        <div class="cs-hero_secondary_title" style="font-size: 21px !important;">Introducing,
        </div>
        <h5 style="font-size: 36px !important;">India's first and most trusted</h5><br/><h4 style="font-size: 36px !important;"> AI POS Software</h4>
        <div class="cs-hero_subtitle">3 Arrows
          A cutting-edge Point of Sale AI software management<br/> company, revolutionizing businesses of all sizes with its advanced <br/>and generative AI-driven solutions.</div>
        <a href="#" class="cs-btn"><span>Start Free Trial</span></a>
      </div>
      <div class="cs-hero_shapes">
        <div class="cs-shape cs-shape_position1">
          <img src="{{asset('public/assets/img/shape/shape_1.svg')}}" alt="Shape">
        </div>
        <div class="cs-shape cs-shape_position2">
          <img src="{{asset('public/assets/img/shape/shape_2.svg')}}" alt="Shape">
        </div>
        <div class="cs-shape cs-shape_position3">
          <img src="{{asset('public/assets/img/shape/shape_3.svg')}}" alt="Shape">
        </div>
        <div class="cs-shape cs-shape_position4">
          <img src="{{asset('public/assets/img/shape/shape_4.svg')}}" alt="Shape">
        </div>
        <div class="cs-shape cs-shape_position5">
          <img src="{{asset('public/assets/img/shape/shape_5.svg')}}" alt="Shape">
        </div>
        <div class="cs-shape cs-shape_position6">
          <img src="{{asset('public/assets/img/shape/shape_6.svg')}}" alt="Shape">
        </div>
      </div>
    </div>
  </section>
</div>
<!-- End Hero -->

<!-- Start Main Feature -->
<section class="cs-bg" data-src="{{asset('public/assets/img/feature_bg.svg')}}">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Office & Inventory</div>
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">Inventory Features</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="row">
      <div class="col-md-6 col-xl-3">
        <div class="cs-height_25 cs-height_lg_0"></div>
        <div class="cs-iconbox cs-style1">
          <div class="cs-iconbox_icon cs-center">
            <img src="{{asset('public/assets/img/icons/icon_box_1.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_in">
            <div class="cs-iconbox_number cs-primary_font">01</div>
            <h3 class="cs-iconbox_title">Recipe management & costing</h3>
            <div class="cs-iconbox_subtitle">Leverage the power of generative AI to optimize recipes, analyze costs, and maximize profitability. Our software gives suggestions based on real-time market data, giving you a competitive edge.</div>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-md-6 col-xl-3">
        <div class="cs-iconbox cs-style1">
          <div class="cs-iconbox_icon cs-center">
            <img src="{{asset('public/assets/img/icons/icon_box_2.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_in">
            <div class="cs-iconbox_number cs-primary_font">02</div>
            <h3 class="cs-iconbox_title">Stock management & inventory control</h3>
            <div class="cs-iconbox_subtitle">Our AI-driven solution takes inventory control to the next level. It utilizes advanced algorithms to predict demand, prevent overstocking or stockouts, and automatically reorder items when neededs</div>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-md-6 col-xl-3">
        <div class="cs-height_25 cs-height_lg_0"></div>
        <div class="cs-iconbox cs-style1">
          <div class="cs-iconbox_icon cs-center">
            <img src="{{asset('public/assets/img/icons/icon_box_3.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_in">
            <div class="cs-iconbox_number cs-primary_font">03</div>
            <h3 class="cs-iconbox_title">Purchase management systems</h3>
            <div class="cs-iconbox_subtitle">Simplify your purchase processes with our AI-powered software. It automates supplier orders, ensuring timely procurement and minimizing the risk of excess or insufficient inventory.</div>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-md-6 col-xl-3">
        <div class="cs-iconbox cs-style1">
          <div class="cs-iconbox_icon cs-center">
            <img src="{{asset('public/assets/img/icons/icon_box_4.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_in">
            <div class="cs-iconbox_number cs-primary_font">04</div>
            <h3 class="cs-iconbox_title">Powerful inventory reporting</h3>
            <div class="cs-iconbox_subtitle">Our software's generative AI capabilities generates comprehensive reports that provide a holistic view of your inventory health, highlighting areas of improvement and helping you make data-driven decisions.</div>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
    </div>
    <div class="cs-height_75 cs-height_lg_45"></div>
  </div>
</section>
<!-- End Main Feature -->

<!-- Start About -->
<section id="about" class="cs-gradient_bg_1">
  <div class="cs-height_100 cs-height_lg_70"></div>
  <div class="container">
    <div class="row align-items-center flex-column-reverse-lg">
      <div class="col-xl-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
        <div class="cs-left_full_width cs-space110">
          <div class="cs-left_sided_img">
            <img src="{{asset('public/assets/img/about_img_1.png')}}" alt="About">
          </div>
        </div>
      </div>
      <div class="col-xl-6">
        <div class="cs-height_0 cs-height_lg_40"></div>
        <div class="cs-seciton_heading cs-style1">
          <div class="cs-section_subtitle">About The AI POS</div>
          <div class="cs-height_10 cs-height_lg_10"></div>
          <h3 class="cs-section_title">Boosting Business Efficiency and Profitability with a AI Point of Sale (POS) System </h3>
        </div>
        <div class="cs-height_20 cs-height_lg_20"></div>

        <div class="cs-height_15 cs-height_lg_15"></div>
        <div class="cs-list_1_wrap">
          <ul class="cs-list cs-style1 cs-mp0">
            <li>
              <span class="cs-list_icon">
                <img src="{{asset('public/assets/img/icons/tick.svg')}}" alt="Tick">
              </span>
              <div class="cs-list_right">
                <h3>Enhanced Transaction Processing</h3>
                <p>The power of generative AI processes transactions with unmatched speed and accuracy. By leveraging advanced algorithms, our software ensures seamless checkouts, minimizes waiting times for customers, and maximizes sales potential, resulting in elevated customer satisfaction and increased revenue.</p>
              </div>
            </li>
            <li>
              <span class="cs-list_icon">
                <img src="{{asset('public/assets/img/icons/tick.svg')}}" alt="Tick">
              </span>
              <div class="cs-list_right">
                <h3>Streamlined Inventory Management</h3>
                <p>Seamless integrated system enables businesses to effortlessly track stock levels, automate reordering, and gain real-time insights into product performance. Our software optimizes inventory control, reduces the risk of stockouts, and enhances overall operational efficiency, leading to cost savings and improved profitability.</p>
              </div>
            </li>
          </ul>
          <div class="cs-list_img wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"><img src="{{asset('public/assets/img/about_img_2.png')}}" alt="About"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cs-height_100 cs-height_lg_70"></div>
  <div class="cs-height_135 cs-height_lg_0"></div>
</section>
<!-- End About -->

<!-- Start Fun Fact -->
<div class="row">
  <div class="cs-funfact_1_wrap cs-type1">
    <div class="cs-funfact cs-style1">
      <div class="cs-funfact_icon cs-center"><img src="{{asset('public/assets/img/icons/funfact_icon_1.svg')}}" alt="Icon"></div>
      <div class="cs-funfact_right">
        <div class="cs-funfact_number cs-primary_font"><span data-count-to="100" class="odometer"></span>+</div>
        <h2 class="cs-funfact_title">Restaurant Outlet</h2>
      </div>
    </div>
    <div class="cs-funfact cs-style1">
      <div class="cs-funfact_icon cs-center"><img src="{{asset('public/assets/img/icons/funfact_icon_2.svg')}}" alt="Icon"></div>
      <div class="cs-funfact_right">
        <div class="cs-funfact_number cs-primary_font"><span data-count-to="300" class="odometer"></span>+</div>
        <h2 class="cs-funfact_title">Customer</h2>
      </div>
    </div>
    <div class="cs-funfact cs-style1">
      <div class="cs-funfact_icon cs-center"><img src="{{asset('public/assets/img/icons/funfact_icon_3.svg')}}" alt="Icon"></div>
      <div class="cs-funfact_right">
        <div class="cs-funfact_number cs-primary_font"><span data-count-to="600" class="odometer"></span>+</div>
        <h2 class="cs-funfact_title">Positive Rating</h2>
      </div>
    </div>
    <div class="cs-funfact cs-style1">
      <div class="cs-funfact_icon cs-center"><img src="{{asset('public/assets/img/icons/funfact_icon_4.svg')}}" alt="Icon"></div>
      <div class="cs-funfact_right">
        <div class="cs-funfact_number cs-primary_font"><span data-count-to="2" class="odometer">
        <h2 class="cs-funfact_title">Award Winning</h2>
      </div>
    </div>
  </div>
</div>
<!-- End Fun Fact -->

<!-- Start All Feature -->
<section class="cs-bg" data-src="{{asset('public/assets/img/feature_bg.svg')}}">
  <div class="cs-height_135 cs-height_lg_0"></div>
  <div id="feature">
    <div class="cs-height_95 cs-height_lg_70"></div>
    <div class="container">
      <div class="cs-seciton_heading cs-style1 text-center">
        <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">AI POS Features</div>
        <div class="cs-height_10 cs-height_lg_10"></div>
        <h3 class="cs-section_title">Available features</h3>
      </div>
      <div class="cs-height_50 cs-height_lg_40"></div>
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_5.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">AI Generative</h3>
              <div class="cs-iconbox_subtitle">is a type of Artificial Intelligence that can create a wide variety of data, such as images & text content.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_6.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">Software Accuracy</h3>
              <div class="cs-iconbox_subtitle">Ensure precision and reliability in all operations.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_7.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">Customization</h3>
              <div class="cs-iconbox_subtitle">Tailor the software to meet specific business needs and preferences.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_8.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">Customer Data</h3>
              <div class="cs-iconbox_subtitle">Centralize customer information for personalized interactions.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_9.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">Seamless checkout</h3>
              <div class="cs-iconbox_subtitle">Swift and hassle-free checkout experience.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="cs-iconbox cs-style1 cs-type1">
            <div class="cs-iconbox_icon cs-center">
              <img src="{{asset('public/assets/img/icons/icon_box_10.svg')}}" alt="Icon">
            </div>
            <div class="cs-iconbox_in">
              <h3 class="cs-iconbox_title">High-Speed Process</h3>
              <div class="cs-iconbox_subtitle">Optimize efficiency with fast processing capabilities.</div>
            </div>
          </div>
          <div class="cs-height_25 cs-height_lg_25"></div>
        </div>
      </div>
    </div>
    <div class="cs-height_75 cs-height_lg_45"></div>
  </div>
</section>
<!-- End All Feature -->
<!-- Start Retail Stores -->
<section class="cs-gradient_bg_1" id="outlettype">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Outlet Type</div>
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">Perfect AI point of sale software for most Outlet types</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="row align-items-center flex-column-reverse-lg">
      <div class="col-xl-6">
        <div class="cs-left_full_width cs-space40">
          <div class="cs-left_sided_img">
            <img src="{{asset('public/assets/img/retail-store.png')}}" alt="Retail Stores">
          </div>
        </div>
      </div>
      <div class="col-xl-6">
        <div class="cs-height_25 cs-height_lg_40"></div>
        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="col-lg-11 offset-lg-1">
            <div class="row">
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_1.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Fine Dine</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_2.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">QSR</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="col-lg-11" style="margin-left: 50px;">
            <div class="row">
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_3.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Cafe</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_4.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Food Court</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="col-lg-11 offset-lg-1">
            <div class="row">
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_5.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Cloud Kitchen</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_6.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Ice Cream</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="col-lg-11 offset-lg-1">
            <div class="row">
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_5.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Bakery</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_6.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Bar & Brewery</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <div class="col-lg-11 offset-lg-1">
            <div class="row">
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_5.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Pizzeria</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
              <div class="col-md-6">
                <div class="cs-iconbox cs-style2 text-center">
                  <div class="cs-iconbox_icon"><img src="{{asset('public/assets/img/icons/retail_icon_6.svg')}}" alt="Icon"></div>
                  <h2 class="cs-iconbox_title">Large Chains</h2>
                </div>
                <div class="cs-height_25 cs-height_lg_25"></div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="cs-height_75 cs-height_lg_70"></div>
</section>
<!-- End Retail Stores -->
<!-- Start Price Section -->
<section class="cs-bg" data-src="{{asset('public/assets/img/feature_bg.svg')}}" id="pricing">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <!-- <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">Pricing Plan</div> -->
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">Founder</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="row">
    <div class="col-lg-12">
        <div class="cs-pricing_table cs-style1">
          <div class="cs-pricing_head">
          </div>
          <div class="cs-pricing_body">
            <ul class="cs-mp0 cs-pricing_feature">
              <li>
                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.18215 0C3.771 0 0.18219 3.58872 0.18219 7.99996C0.18219 12.4112 3.771 16 8.18215 16C12.5933 16 16.1821 12.4112 16.1821 7.99996C16.1821 3.58872 12.5934 0 8.18215 0ZM12.7759 6.6487L7.7499 11.6747C7.53619 11.8884 7.25212 12.006 6.94993 12.006C6.64774 12.006 6.36366 11.8884 6.14996 11.6747L3.58843 9.11314C3.37473 8.89944 3.25702 8.61536 3.25702 8.31317C3.25702 8.01089 3.37473 7.72682 3.58843 7.51311C3.80205 7.29941 4.08613 7.18171 4.3884 7.18171C4.69059 7.18171 4.97475 7.29941 5.18837 7.5132L6.94984 9.27459L11.1758 5.04867C11.3895 4.83497 11.6735 4.71735 11.9757 4.71735C12.2779 4.71735 12.562 4.83497 12.7757 5.04867C13.217 5.48994 13.217 6.2076 12.7759 6.6487Z" fill="#47C4C0" />
                </svg>
                <span>Established in 2023, 3 Arrows is emerging as the largest AI-powered Point of Sale (PoS) platform for the Food and Beverage (F&B) sector. Our platform is available in 128 languages, catering to a global customer base.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-lg-12">
        <div class="cs-pricing_table cs-style1">
          <div class="cs-pricing_head">
          </div>
          <div class="cs-pricing_body">
            <ul class="cs-mp0 cs-pricing_feature">
              <li>
                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.18215 0C3.771 0 0.18219 3.58872 0.18219 7.99996C0.18219 12.4112 3.771 16 8.18215 16C12.5933 16 16.1821 12.4112 16.1821 7.99996C16.1821 3.58872 12.5934 0 8.18215 0ZM12.7759 6.6487L7.7499 11.6747C7.53619 11.8884 7.25212 12.006 6.94993 12.006C6.64774 12.006 6.36366 11.8884 6.14996 11.6747L3.58843 9.11314C3.37473 8.89944 3.25702 8.61536 3.25702 8.31317C3.25702 8.01089 3.37473 7.72682 3.58843 7.51311C3.80205 7.29941 4.08613 7.18171 4.3884 7.18171C4.69059 7.18171 4.97475 7.29941 5.18837 7.5132L6.94984 9.27459L11.1758 5.04867C11.3895 4.83497 11.6735 4.71735 11.9757 4.71735C12.2779 4.71735 12.562 4.83497 12.7757 5.04867C13.217 5.48994 13.217 6.2076 12.7759 6.6487Z" fill="#47C4C0" />
                </svg>
                <span>At 3 Arrows, We offer a comprehensive restaurant management software that encompasses various aspects such as billing, inventory management, online ordering, and all other essential front and back-of-house management modules. Our software is designed to cater to the needs of diverse establishments, from standalone restaurants to restaurant chains.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>  
    <div class="col-lg-12">
        <div class="cs-pricing_table cs-style1">
          <div class="cs-pricing_head">
          </div>
          <div class="cs-pricing_body">
            <ul class="cs-mp0 cs-pricing_feature">
              <li>
                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.18215 0C3.771 0 0.18219 3.58872 0.18219 7.99996C0.18219 12.4112 3.771 16 8.18215 16C12.5933 16 16.1821 12.4112 16.1821 7.99996C16.1821 3.58872 12.5934 0 8.18215 0ZM12.7759 6.6487L7.7499 11.6747C7.53619 11.8884 7.25212 12.006 6.94993 12.006C6.64774 12.006 6.36366 11.8884 6.14996 11.6747L3.58843 9.11314C3.37473 8.89944 3.25702 8.61536 3.25702 8.31317C3.25702 8.01089 3.37473 7.72682 3.58843 7.51311C3.80205 7.29941 4.08613 7.18171 4.3884 7.18171C4.69059 7.18171 4.97475 7.29941 5.18837 7.5132L6.94984 9.27459L11.1758 5.04867C11.3895 4.83497 11.6735 4.71735 11.9757 4.71735C12.2779 4.71735 12.562 4.83497 12.7757 5.04867C13.217 5.48994 13.217 6.2076 12.7759 6.6487Z" fill="#47C4C0" />
                </svg>
                <span>GoldGoldBrings a wealth of experience to the table, with a combined tenure of over 7 years in the fields of technology, brand building, marketing, and customer relationship management. His expertise ensures our platform is designed to meet the specific requirements of the F&B industry.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-lg-12">
        <div class="cs-pricing_table cs-style1">
          <div class="cs-pricing_head">
          </div>
          <div class="cs-pricing_body">
            <ul class="cs-mp0 cs-pricing_feature">
             
              <li>
                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.18215 0C3.771 0 0.18219 3.58872 0.18219 7.99996C0.18219 12.4112 3.771 16 8.18215 16C12.5933 16 16.1821 12.4112 16.1821 7.99996C16.1821 3.58872 12.5934 0 8.18215 0ZM12.7759 6.6487L7.7499 11.6747C7.53619 11.8884 7.25212 12.006 6.94993 12.006C6.64774 12.006 6.36366 11.8884 6.14996 11.6747L3.58843 9.11314C3.37473 8.89944 3.25702 8.61536 3.25702 8.31317C3.25702 8.01089 3.37473 7.72682 3.58843 7.51311C3.80205 7.29941 4.08613 7.18171 4.3884 7.18171C4.69059 7.18171 4.97475 7.29941 5.18837 7.5132L6.94984 9.27459L11.1758 5.04867C11.3895 4.83497 11.6735 4.71735 11.9757 4.71735C12.2779 4.71735 12.562 4.83497 12.7757 5.04867C13.217 5.48994 13.217 6.2076 12.7759 6.6487Z" fill="#47C4C0" />
                </svg>
                <span>We have formed strong partnerships with more than 300 companies and organizations, including renowned brands such as Lava mobiles, Faces Canada, Lodha, Raymond, Bajaj Finance, and JBL, among others. These collaborations reflect our commitment to delivering top-notch solutions and our ability to forge valuable relationships within the industry.</span>
              </li>
             
            </ul>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>
      <div class="col-lg-12">
        <div class="cs-pricing_table cs-style1">
          <div class="cs-pricing_head">
          </div>
          <div class="cs-pricing_body">
            <ul class="cs-mp0 cs-pricing_feature">
             
              <li class="cs-active">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M7.99583 0C3.57967 0 0 3.58154 0 8C0 12.4185 3.57967 16 7.99583 16C12.412 16 15.9917 12.4185 15.9917 8C15.9917 3.58154 12.412 0 7.99583 0ZM6.58529 5.71897C6.52898 5.65851 6.46108 5.61002 6.38563 5.57639C6.31018 5.54275 6.22873 5.52466 6.14615 5.52321C6.06356 5.52175 5.98153 5.53695 5.90495 5.5679C5.82836 5.59885 5.75879 5.64492 5.70038 5.70335C5.64198 5.76179 5.59593 5.8314 5.565 5.90802C5.53407 5.98465 5.51887 6.06672 5.52033 6.14935C5.52179 6.23198 5.53986 6.31347 5.57348 6.38895C5.6071 6.46444 5.65557 6.53238 5.716 6.58872L7.12654 8L5.716 9.41128C5.65557 9.46762 5.6071 9.53556 5.57348 9.61105C5.53986 9.68653 5.52179 9.76802 5.52033 9.85065C5.51887 9.93328 5.53407 10.0154 5.565 10.092C5.59593 10.1686 5.64198 10.2382 5.70038 10.2966C5.75879 10.3551 5.82836 10.4012 5.90495 10.4321C5.98153 10.4631 6.06356 10.4783 6.14615 10.4768C6.22873 10.4753 6.31018 10.4572 6.38563 10.4236C6.46108 10.39 6.52898 10.3415 6.58529 10.281L7.99583 8.86974L9.40638 10.281C9.46269 10.3415 9.53059 10.39 9.60604 10.4236C9.68149 10.4572 9.76293 10.4753 9.84552 10.4768C9.92811 10.4783 10.0101 10.4631 10.0867 10.4321C10.1633 10.4012 10.2329 10.3551 10.2913 10.2966C10.3497 10.2382 10.3957 10.1686 10.4267 10.092C10.4576 10.0154 10.4728 9.93328 10.4713 9.85065C10.4699 9.76802 10.4518 9.68653 10.4182 9.61105C10.3846 9.53556 10.3361 9.46762 10.2757 9.41128L8.86513 8L10.2757 6.58872C10.3361 6.53238 10.3846 6.46444 10.4182 6.38895C10.4518 6.31347 10.4699 6.23198 10.4713 6.14935C10.4728 6.06672 10.4576 5.98465 10.4267 5.90802C10.3957 5.8314 10.3497 5.76179 10.2913 5.70335C10.2329 5.64492 10.1633 5.59885 10.0867 5.5679C10.0101 5.53695 9.92811 5.52175 9.84552 5.52321C9.76293 5.52466 9.68149 5.54275 9.60604 5.57639C9.53059 5.61002 9.46269 5.65851 9.40638 5.71897L7.99583 7.13026L6.58529 5.71897Z" fill="#47C4C0" />
                </svg>
                <span>At 3 arrows, we are dedicated to revolutionizing the restaurant management landscape through cutting-edge AI technology, unparalleled customer support, and seamless integration of key features. Our aim is to empower restaurants with the tools they need to thrive in an ever-evolving industry starting in 3 months.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="cs-height_25 cs-height_lg_25"></div>
      </div>

    </div>
    <div class="cs-height_75 cs-height_lg_45"></div>
  </div>
</section>
<!-- End Price Section -->
<!-- End Testimonials Section -->
<section class="cs-gradient_bg_1">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">Testimonials</div>
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">What our client’s say</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="cs-slider cs-style1 cs-gap-24">
      <div class="cs-slider_container" data-autoplay="0" data-loop="1" data-speed="600" data-fade-slide="0" data-slides-per-view="responsive" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lg-slides="3" data-add-slides="3">
        <div class="cs-slider_wrapper">
          <div class="cs-slide">
            <div class="cs-testimonial cs-style1">
              <div class="cs-testimonial_text">Our sales process is now smooth, and customer friendly. The system is so easy to use, and the checkout experience is seamless. Trust me, you won't regret choosing them!.</div>
              <div class="cs-testimonial_meta">
                <div class="cs-avatar">
                  <img src="{{asset('public/assets/img/avatar_1.png')}}" alt="Avatar">
                  <div class="cs-quote cs-center">
                    <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M17.9033 0.470251C17.974 0.470231 18.0431 0.492568 18.1019 0.534429C18.1606 0.576288 18.2064 0.635782 18.2333 0.705356L18.9732 2.62083C19.0075 2.70954 19.0089 2.80875 18.9772 2.89853C18.9455 2.98831 18.8831 3.06201 18.8024 3.1048C17.5346 3.77738 16.6023 4.83665 16.0311 6.25297C15.8289 6.75453 15.6825 7.27956 15.595 7.81733L17.569 7.81733C17.6636 7.81733 17.7544 7.85732 17.8213 7.92851C17.8882 7.9997 17.9258 8.09624 17.9258 8.19692L17.9258 14.6207C17.9258 14.7213 17.8882 14.8179 17.8213 14.8891C17.7544 14.9603 17.6636 15.0002 17.569 15.0002L10.2806 15.0002C10.1859 15.0002 10.0952 14.9603 10.0283 14.8891C9.96136 14.8179 9.92377 14.7213 9.92377 14.6207L9.92377 10.2215C9.92329 8.5455 10.3244 6.897 11.0891 5.4317C11.8539 3.9664 12.9571 2.73265 14.2946 1.84695C15.2002 1.24811 16.1969 0.821584 17.24 0.586498C17.6221 0.499905 17.8567 0.473809 17.8665 0.472623C17.8787 0.47112 17.891 0.470328 17.9033 0.470251ZM18.19 2.59094L17.6794 1.26856C17.5984 1.28351 17.501 1.3032 17.3895 1.32859C13.4762 2.2142 10.6374 5.95429 10.6374 10.2215L10.6374 14.2411L17.2122 14.2411L17.2122 8.5765L15.1902 8.5765C15.1417 8.57658 15.0936 8.56612 15.049 8.54576C15.0044 8.52541 14.9642 8.49559 14.9308 8.45813C14.8974 8.42066 14.8715 8.37634 14.8548 8.32788C14.838 8.27942 14.8308 8.22783 14.8334 8.17628C14.8354 8.13571 14.8892 7.17133 15.3635 5.98228C15.6341 5.29767 16.0027 4.66176 16.4562 4.09717C16.9467 3.49086 17.5336 2.98102 18.19 2.59094Z" fill="currentColor" />
                      <path d="M8.42661 0.470251C8.4973 0.470278 8.56638 0.492638 8.6251 0.534494C8.68382 0.576352 8.72953 0.635819 8.75643 0.705356L9.49636 2.62083C9.53062 2.70954 9.53205 2.80875 9.50036 2.89853C9.46867 2.98831 9.40621 3.06201 9.32554 3.1048C8.05777 3.77738 7.12539 4.83665 6.55428 6.25297C6.3522 6.75461 6.20582 7.27961 6.11809 7.81733L8.09211 7.81733C8.18674 7.81733 8.27749 7.85732 8.34441 7.92851C8.41132 7.9997 8.44891 8.09624 8.44891 8.19692L8.44891 14.6207C8.44891 14.7213 8.41132 14.8179 8.34441 14.8891C8.27749 14.9603 8.18674 15.0002 8.09211 15.0002L0.802814 15.0002C0.708182 15.0002 0.617428 14.9603 0.550514 14.8891C0.483601 14.8179 0.446009 14.7213 0.446009 14.6207L0.446009 10.2215C0.442028 7.96623 1.16694 5.77803 2.49922 4.02375C3.8315 2.26946 5.69016 1.05573 7.76363 0.586024C8.14563 0.499431 8.38001 0.473334 8.38982 0.472148C8.40204 0.470806 8.41432 0.470174 8.42661 0.470251ZM8.71339 2.59094L8.2025 1.26856C8.12177 1.28351 8.02409 1.3032 7.91259 1.32859C3.99934 2.2142 1.15962 5.95429 1.15962 10.2215L1.15962 14.2411L7.7353 14.2411L7.7353 8.5765L5.71334 8.5765C5.6648 8.57658 5.61676 8.56612 5.57216 8.54576C5.52755 8.52541 5.48732 8.49559 5.45392 8.45813C5.42052 8.42066 5.39466 8.37634 5.37791 8.32788C5.36117 8.27942 5.35389 8.22783 5.35653 8.17628C5.35854 8.13571 5.41251 7.17133 5.88661 5.98228C6.15719 5.29767 6.5258 4.66177 6.97932 4.09717C7.46979 3.49073 8.05677 2.98087 8.71339 2.59094Z" fill="currentColor" />
                    </svg>
                  </div>
                </div>
                <div class="cs-testimonial_meta_right">
                  <h3>Edward Wolfe</h3>
                  <p>Customer</p>
                  <div class="cs-review" data-review="4">
                    <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    <div class="cs-review_in">
                      <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-testimonial cs-style1">
              <div class="cs-testimonial_text">Guys, you've got to check out 3 Arrows POS! It's been a total game-changer for our restaurant. We can manage our inventory like never before, and the best part? We can customize it to fit our exact needs.</div>
              <div class="cs-testimonial_meta">
                <div class="cs-avatar">
                  <img src="{{asset('public/assets/img/avatar_2.png')}}" alt="Avatar">
                  <div class="cs-quote cs-center">
                    <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M17.9033 0.470251C17.974 0.470231 18.0431 0.492568 18.1019 0.534429C18.1606 0.576288 18.2064 0.635782 18.2333 0.705356L18.9732 2.62083C19.0075 2.70954 19.0089 2.80875 18.9772 2.89853C18.9455 2.98831 18.8831 3.06201 18.8024 3.1048C17.5346 3.77738 16.6023 4.83665 16.0311 6.25297C15.8289 6.75453 15.6825 7.27956 15.595 7.81733L17.569 7.81733C17.6636 7.81733 17.7544 7.85732 17.8213 7.92851C17.8882 7.9997 17.9258 8.09624 17.9258 8.19692L17.9258 14.6207C17.9258 14.7213 17.8882 14.8179 17.8213 14.8891C17.7544 14.9603 17.6636 15.0002 17.569 15.0002L10.2806 15.0002C10.1859 15.0002 10.0952 14.9603 10.0283 14.8891C9.96136 14.8179 9.92377 14.7213 9.92377 14.6207L9.92377 10.2215C9.92329 8.5455 10.3244 6.897 11.0891 5.4317C11.8539 3.9664 12.9571 2.73265 14.2946 1.84695C15.2002 1.24811 16.1969 0.821584 17.24 0.586498C17.6221 0.499905 17.8567 0.473809 17.8665 0.472623C17.8787 0.47112 17.891 0.470328 17.9033 0.470251ZM18.19 2.59094L17.6794 1.26856C17.5984 1.28351 17.501 1.3032 17.3895 1.32859C13.4762 2.2142 10.6374 5.95429 10.6374 10.2215L10.6374 14.2411L17.2122 14.2411L17.2122 8.5765L15.1902 8.5765C15.1417 8.57658 15.0936 8.56612 15.049 8.54576C15.0044 8.52541 14.9642 8.49559 14.9308 8.45813C14.8974 8.42066 14.8715 8.37634 14.8548 8.32788C14.838 8.27942 14.8308 8.22783 14.8334 8.17628C14.8354 8.13571 14.8892 7.17133 15.3635 5.98228C15.6341 5.29767 16.0027 4.66176 16.4562 4.09717C16.9467 3.49086 17.5336 2.98102 18.19 2.59094Z" fill="currentColor" />
                      <path d="M8.42661 0.470251C8.4973 0.470278 8.56638 0.492638 8.6251 0.534494C8.68382 0.576352 8.72953 0.635819 8.75643 0.705356L9.49636 2.62083C9.53062 2.70954 9.53205 2.80875 9.50036 2.89853C9.46867 2.98831 9.40621 3.06201 9.32554 3.1048C8.05777 3.77738 7.12539 4.83665 6.55428 6.25297C6.3522 6.75461 6.20582 7.27961 6.11809 7.81733L8.09211 7.81733C8.18674 7.81733 8.27749 7.85732 8.34441 7.92851C8.41132 7.9997 8.44891 8.09624 8.44891 8.19692L8.44891 14.6207C8.44891 14.7213 8.41132 14.8179 8.34441 14.8891C8.27749 14.9603 8.18674 15.0002 8.09211 15.0002L0.802814 15.0002C0.708182 15.0002 0.617428 14.9603 0.550514 14.8891C0.483601 14.8179 0.446009 14.7213 0.446009 14.6207L0.446009 10.2215C0.442028 7.96623 1.16694 5.77803 2.49922 4.02375C3.8315 2.26946 5.69016 1.05573 7.76363 0.586024C8.14563 0.499431 8.38001 0.473334 8.38982 0.472148C8.40204 0.470806 8.41432 0.470174 8.42661 0.470251ZM8.71339 2.59094L8.2025 1.26856C8.12177 1.28351 8.02409 1.3032 7.91259 1.32859C3.99934 2.2142 1.15962 5.95429 1.15962 10.2215L1.15962 14.2411L7.7353 14.2411L7.7353 8.5765L5.71334 8.5765C5.6648 8.57658 5.61676 8.56612 5.57216 8.54576C5.52755 8.52541 5.48732 8.49559 5.45392 8.45813C5.42052 8.42066 5.39466 8.37634 5.37791 8.32788C5.36117 8.27942 5.35389 8.22783 5.35653 8.17628C5.35854 8.13571 5.41251 7.17133 5.88661 5.98228C6.15719 5.29767 6.5258 4.66177 6.97932 4.09717C7.46979 3.49073 8.05677 2.98087 8.71339 2.59094Z" fill="currentColor" />
                    </svg>
                  </div>
                </div>
                <div class="cs-testimonial_meta_right">
                  <h3>Rodney Bryner</h3>
                  <p>Customer</p>
                  <div class="cs-review" data-review="5">
                    <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    <div class="cs-review_in">
                      <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-testimonial cs-style1">
              <div class="cs-testimonial_text">Their powerful reporting feature has given us incredible insights. We make smarter decisions now. And let me tell you, their customer support team is top-notch. Always there to help when we need them. You won't find a better option!.</div>
              <div class="cs-testimonial_meta">
                <div class="cs-avatar">
                  <img src="{{asset('public/assets/img/avatar_3.png')}}" alt="Avatar">
                  <div class="cs-quote cs-center">
                    <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M17.9033 0.470251C17.974 0.470231 18.0431 0.492568 18.1019 0.534429C18.1606 0.576288 18.2064 0.635782 18.2333 0.705356L18.9732 2.62083C19.0075 2.70954 19.0089 2.80875 18.9772 2.89853C18.9455 2.98831 18.8831 3.06201 18.8024 3.1048C17.5346 3.77738 16.6023 4.83665 16.0311 6.25297C15.8289 6.75453 15.6825 7.27956 15.595 7.81733L17.569 7.81733C17.6636 7.81733 17.7544 7.85732 17.8213 7.92851C17.8882 7.9997 17.9258 8.09624 17.9258 8.19692L17.9258 14.6207C17.9258 14.7213 17.8882 14.8179 17.8213 14.8891C17.7544 14.9603 17.6636 15.0002 17.569 15.0002L10.2806 15.0002C10.1859 15.0002 10.0952 14.9603 10.0283 14.8891C9.96136 14.8179 9.92377 14.7213 9.92377 14.6207L9.92377 10.2215C9.92329 8.5455 10.3244 6.897 11.0891 5.4317C11.8539 3.9664 12.9571 2.73265 14.2946 1.84695C15.2002 1.24811 16.1969 0.821584 17.24 0.586498C17.6221 0.499905 17.8567 0.473809 17.8665 0.472623C17.8787 0.47112 17.891 0.470328 17.9033 0.470251ZM18.19 2.59094L17.6794 1.26856C17.5984 1.28351 17.501 1.3032 17.3895 1.32859C13.4762 2.2142 10.6374 5.95429 10.6374 10.2215L10.6374 14.2411L17.2122 14.2411L17.2122 8.5765L15.1902 8.5765C15.1417 8.57658 15.0936 8.56612 15.049 8.54576C15.0044 8.52541 14.9642 8.49559 14.9308 8.45813C14.8974 8.42066 14.8715 8.37634 14.8548 8.32788C14.838 8.27942 14.8308 8.22783 14.8334 8.17628C14.8354 8.13571 14.8892 7.17133 15.3635 5.98228C15.6341 5.29767 16.0027 4.66176 16.4562 4.09717C16.9467 3.49086 17.5336 2.98102 18.19 2.59094Z" fill="currentColor" />
                      <path d="M8.42661 0.470251C8.4973 0.470278 8.56638 0.492638 8.6251 0.534494C8.68382 0.576352 8.72953 0.635819 8.75643 0.705356L9.49636 2.62083C9.53062 2.70954 9.53205 2.80875 9.50036 2.89853C9.46867 2.98831 9.40621 3.06201 9.32554 3.1048C8.05777 3.77738 7.12539 4.83665 6.55428 6.25297C6.3522 6.75461 6.20582 7.27961 6.11809 7.81733L8.09211 7.81733C8.18674 7.81733 8.27749 7.85732 8.34441 7.92851C8.41132 7.9997 8.44891 8.09624 8.44891 8.19692L8.44891 14.6207C8.44891 14.7213 8.41132 14.8179 8.34441 14.8891C8.27749 14.9603 8.18674 15.0002 8.09211 15.0002L0.802814 15.0002C0.708182 15.0002 0.617428 14.9603 0.550514 14.8891C0.483601 14.8179 0.446009 14.7213 0.446009 14.6207L0.446009 10.2215C0.442028 7.96623 1.16694 5.77803 2.49922 4.02375C3.8315 2.26946 5.69016 1.05573 7.76363 0.586024C8.14563 0.499431 8.38001 0.473334 8.38982 0.472148C8.40204 0.470806 8.41432 0.470174 8.42661 0.470251ZM8.71339 2.59094L8.2025 1.26856C8.12177 1.28351 8.02409 1.3032 7.91259 1.32859C3.99934 2.2142 1.15962 5.95429 1.15962 10.2215L1.15962 14.2411L7.7353 14.2411L7.7353 8.5765L5.71334 8.5765C5.6648 8.57658 5.61676 8.56612 5.57216 8.54576C5.52755 8.52541 5.48732 8.49559 5.45392 8.45813C5.42052 8.42066 5.39466 8.37634 5.37791 8.32788C5.36117 8.27942 5.35389 8.22783 5.35653 8.17628C5.35854 8.13571 5.41251 7.17133 5.88661 5.98228C6.15719 5.29767 6.5258 4.66177 6.97932 4.09717C7.46979 3.49073 8.05677 2.98087 8.71339 2.59094Z" fill="currentColor" />
                    </svg>
                  </div>
                </div>
                <div class="cs-testimonial_meta_right">
                  <h3>Jacque Askew</h3>
                  <p>Customer</p>
                  <div class="cs-review" data-review="4.5">
                    <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    <div class="cs-review_in">
                      <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-testimonial cs-style1">
              <div class="cs-testimonial_text">With Thrive’s help, we were able to increase the functionality of our website dramatically while were able to increase the.</div>
              <div class="cs-testimonial_meta">
                <div class="cs-avatar">
                  <img src="{{asset('public/assets/img/avatar_1.png')}}" alt="Avatar">
                  <div class="cs-quote cs-center">
                    <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M17.9033 0.470251C17.974 0.470231 18.0431 0.492568 18.1019 0.534429C18.1606 0.576288 18.2064 0.635782 18.2333 0.705356L18.9732 2.62083C19.0075 2.70954 19.0089 2.80875 18.9772 2.89853C18.9455 2.98831 18.8831 3.06201 18.8024 3.1048C17.5346 3.77738 16.6023 4.83665 16.0311 6.25297C15.8289 6.75453 15.6825 7.27956 15.595 7.81733L17.569 7.81733C17.6636 7.81733 17.7544 7.85732 17.8213 7.92851C17.8882 7.9997 17.9258 8.09624 17.9258 8.19692L17.9258 14.6207C17.9258 14.7213 17.8882 14.8179 17.8213 14.8891C17.7544 14.9603 17.6636 15.0002 17.569 15.0002L10.2806 15.0002C10.1859 15.0002 10.0952 14.9603 10.0283 14.8891C9.96136 14.8179 9.92377 14.7213 9.92377 14.6207L9.92377 10.2215C9.92329 8.5455 10.3244 6.897 11.0891 5.4317C11.8539 3.9664 12.9571 2.73265 14.2946 1.84695C15.2002 1.24811 16.1969 0.821584 17.24 0.586498C17.6221 0.499905 17.8567 0.473809 17.8665 0.472623C17.8787 0.47112 17.891 0.470328 17.9033 0.470251ZM18.19 2.59094L17.6794 1.26856C17.5984 1.28351 17.501 1.3032 17.3895 1.32859C13.4762 2.2142 10.6374 5.95429 10.6374 10.2215L10.6374 14.2411L17.2122 14.2411L17.2122 8.5765L15.1902 8.5765C15.1417 8.57658 15.0936 8.56612 15.049 8.54576C15.0044 8.52541 14.9642 8.49559 14.9308 8.45813C14.8974 8.42066 14.8715 8.37634 14.8548 8.32788C14.838 8.27942 14.8308 8.22783 14.8334 8.17628C14.8354 8.13571 14.8892 7.17133 15.3635 5.98228C15.6341 5.29767 16.0027 4.66176 16.4562 4.09717C16.9467 3.49086 17.5336 2.98102 18.19 2.59094Z" fill="currentColor" />
                      <path d="M8.42661 0.470251C8.4973 0.470278 8.56638 0.492638 8.6251 0.534494C8.68382 0.576352 8.72953 0.635819 8.75643 0.705356L9.49636 2.62083C9.53062 2.70954 9.53205 2.80875 9.50036 2.89853C9.46867 2.98831 9.40621 3.06201 9.32554 3.1048C8.05777 3.77738 7.12539 4.83665 6.55428 6.25297C6.3522 6.75461 6.20582 7.27961 6.11809 7.81733L8.09211 7.81733C8.18674 7.81733 8.27749 7.85732 8.34441 7.92851C8.41132 7.9997 8.44891 8.09624 8.44891 8.19692L8.44891 14.6207C8.44891 14.7213 8.41132 14.8179 8.34441 14.8891C8.27749 14.9603 8.18674 15.0002 8.09211 15.0002L0.802814 15.0002C0.708182 15.0002 0.617428 14.9603 0.550514 14.8891C0.483601 14.8179 0.446009 14.7213 0.446009 14.6207L0.446009 10.2215C0.442028 7.96623 1.16694 5.77803 2.49922 4.02375C3.8315 2.26946 5.69016 1.05573 7.76363 0.586024C8.14563 0.499431 8.38001 0.473334 8.38982 0.472148C8.40204 0.470806 8.41432 0.470174 8.42661 0.470251ZM8.71339 2.59094L8.2025 1.26856C8.12177 1.28351 8.02409 1.3032 7.91259 1.32859C3.99934 2.2142 1.15962 5.95429 1.15962 10.2215L1.15962 14.2411L7.7353 14.2411L7.7353 8.5765L5.71334 8.5765C5.6648 8.57658 5.61676 8.56612 5.57216 8.54576C5.52755 8.52541 5.48732 8.49559 5.45392 8.45813C5.42052 8.42066 5.39466 8.37634 5.37791 8.32788C5.36117 8.27942 5.35389 8.22783 5.35653 8.17628C5.35854 8.13571 5.41251 7.17133 5.88661 5.98228C6.15719 5.29767 6.5258 4.66177 6.97932 4.09717C7.46979 3.49073 8.05677 2.98087 8.71339 2.59094Z" fill="currentColor" />
                    </svg>
                  </div>
                </div>
                <div class="cs-testimonial_meta_right">
                  <h3>Edward Wolfe</h3>
                  <p>Customer</p>
                  <div class="cs-review" data-review="4">
                    <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    <div class="cs-review_in">
                      <img src="{{asset('public/assets/img/icons/stars.svg')}}" alt="Star">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- .cs-slide -->
        </div>
      </div><!-- .cs-slider_container -->
      <div class="cs-pagination cs-style1"></div>
    </div><!-- .cs-slider -->
  </div>
  <div class="cs-height_100 cs-height_lg_70"></div>
</section>
<!-- End Testimonials Stores -->

<!-- End Client Section -->
<section class="cs-bg" data-src="{{asset('public/assets/img/feature_bg.svg')}}')}}">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <div class="cs-section_subtitle">Our Client</div>
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">Who we partner with</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="cs-slider cs-style1 cs-gap-24">
      <div class="cs-slider_container" data-autoplay="0" data-loop="1" data-speed="600" data-fade-slide="0" data-slides-per-view="responsive" data-xs-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="6" data-add-slides="6">
        <div class="cs-slider_wrapper">
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_1.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_2.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_3.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_4.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_5.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_6.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-client cs-accent_bg cs-center">
              <img src="{{asset('public/assets/img/client_3.svg')}}" alt="Client">
            </div>
          </div><!-- .cs-slide -->
        </div>
      </div><!-- .cs-slider_container -->
      <div class="cs-pagination cs-style1"></div>
    </div><!-- .cs-slider -->
  </div>
  <div class="cs-height_100 cs-height_lg_70"></div>
</section>
<!-- End Client Stores -->

<!-- Start FAQ -->
<section id="faq" class="cs-gradient_bg_1">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="cs-seciton_heading cs-style1 text-center">
    <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">AI POS FAQ</div>
    <div class="cs-height_10 cs-height_lg_10"></div>
    <h3 class="cs-section_title">Frequently asked questions</h3>
  </div>
  <div class="cs-height_50 cs-height_lg_40"></div>
  <div class="container">
    <div class="row align-items-center flex-column-reverse-lg">
      <div class="col-xl-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
        <div class="cs-left_full_width cs-space110">
          <div class="cs-left_sided_img">
            <img src="{{asset('public/assets/img/faq_img.png')}}" alt="About">
          </div>
        </div>
        <div class="cs-height_0 cs-height_lg_40"></div>
      </div>
      <div class="col-xl-6">
        <div class="cs-accordians cs-style1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
          <div class="cs-accordian cs-white_bg active">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q1.</span> What is a Point of Sale (POS) system? </h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              A Point of Sale system is a software and hardware solution that enables businesses to process transactions, manage inventory, and track sales. It includes features like barcode scanning, receipt printing, and sales reporting.
            </div>
          </div><!-- .cs-accordian -->
          <div class="cs-height_25 cs-height_lg_25"></div>
          <div class="cs-accordian cs-white_bg">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q2.</span> How can a POS system benefit my business?</h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              A POS system can streamline your operations, improve efficiency, and boost profitability. It helps automate tasks like inventory management, sales tracking, and customer management, while providing valuable insights for informed decision-making.
            </div>
          </div><!-- .cs-accordian -->
          <div class="cs-height_25 cs-height_lg_25"></div>
          <div class="cs-accordian cs-white_bg">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q3.</span> Can I integrate a POS system with my existing hardware?</h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              Yes, most modern POS systems are designed to be compatible with various hardware setups, including barcode scanners, cash registers, and receipt printers. It's important to check the compatibility of the POS system with your existing hardware before making a purchase.
            </div>
          </div><!-- .cs-accordian -->
          <div class="cs-height_25 cs-height_lg_25"></div>
          <div class="cs-accordian cs-white_bg">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q4.</span> Is my business data secure with a POS system?</h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              Yes, reputable POS systems prioritize data security. Look for features like data encryption, secure payment processing, and user access controls to ensure the safety of your business and customer data.
            </div>
          </div><!-- .cs-accordian -->
          <div class="cs-height_25 cs-height_lg_25"></div>
          <div class="cs-accordian cs-white_bg">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q5.</span> Can I access my business data remotely with a POS system?</h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              Many POS systems offer cloud-based solutions that allow you to access your business data from anywhere with an internet connection. This flexibility enables you to manage your business on the go and stay informed even when you're not physically present.
            </div>
          </div><!-- .cs-accordian -->
          <!-- .cs-accordian -->
          <div class="cs-height_25 cs-height_lg_25"></div>
          <div class="cs-accordian cs-white_bg">
            <div class="cs-accordian_head">
              <h2 class="cs-accordian_title"><span>Q6.</span> How long does it take to set up a POS system?</h2>
              <span class="cs-accordian_toggle">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 -7.36618e-07C12.0333 -8.82307e-07 9.13319 0.879733 6.66645 2.52795C4.19971 4.17617 2.27713 6.51885 1.14181 9.25975C0.00649787 12.0006 -0.290551 15.0166 0.288226 17.9264C0.867005 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16393 29.133 12.0736 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C29.9957 11.0231 28.414 7.21026 25.6019 4.39815C22.7897 1.58603 18.9769 0.00430081 15 -7.36618e-07V-7.36618e-07ZM15 20C14.085 20.0009 13.2014 19.6665 12.5163 19.06C12.1075 18.6962 11.72 18.3425 11.4663 18.0887L7.875 14.5587C7.75017 14.4457 7.64946 14.3086 7.57892 14.1557C7.50838 14.0028 7.46947 13.8372 7.46452 13.6689C7.45957 13.5005 7.48869 13.3329 7.55012 13.1762C7.61155 13.0194 7.70402 12.8766 7.822 12.7564C7.93998 12.6362 8.08102 12.5412 8.23667 12.4769C8.3923 12.4125 8.55934 12.3804 8.72773 12.3822C8.89612 12.3841 9.0624 12.4199 9.21659 12.4876C9.37078 12.5553 9.5097 12.6535 9.62501 12.7762L13.225 16.3125C13.46 16.5462 13.81 16.8637 14.1738 17.1875C14.4021 17.3889 14.6961 17.5001 15.0006 17.5001C15.3051 17.5001 15.5991 17.3889 15.8275 17.1875C16.19 16.865 16.54 16.5475 16.7675 16.3212L20.375 12.7762C20.4903 12.6535 20.6292 12.5553 20.7834 12.4876C20.9376 12.4199 21.1039 12.3841 21.2723 12.3822C21.4407 12.3804 21.6077 12.4125 21.7633 12.4769C21.919 12.5412 22.06 12.6362 22.178 12.7564C22.296 12.8766 22.3885 13.0194 22.4499 13.1762C22.5113 13.333 22.5404 13.5005 22.5355 13.6689C22.5305 13.8372 22.4916 14.0028 22.4211 14.1557C22.3505 14.3086 22.2498 14.4457 22.125 14.5587L18.5263 18.095C18.2763 18.345 17.8925 18.695 17.485 19.0562C16.8003 19.6647 15.916 20.0006 15 20Z" fill="currentColor" />
                </svg>
              </span>
            </div>
            <div class="cs-accordian-body">
              How long does it take to set up a POS system? A: The time required to set up a POS system can vary depending on the complexity of your business and the chosen solution. However, modern POS systems are designed to be user-friendly and can typically be set up relatively quickly.
            </div>
          </div><!-- .cs-accordian -->
        </div>
      </div>
    </div>
  </div>
  <div class="cs-height_100 cs-height_lg_70"></div>
</section>
<!-- End FAQ -->


<!-- End Post Section -->
<section class="cs-bg" data-src="{{asset('public/assets/img/feature_bg.svg')}}" id="news">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="cs-seciton_heading cs-style1 text-center">
      <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">Letest News</div>
      <div class="cs-height_10 cs-height_lg_10"></div>
      <h3 class="cs-section_title">Point of sale latest news</h3>
    </div>
    <div class="cs-height_50 cs-height_lg_40"></div>
    <div class="cs-slider cs-style1 cs-gap-24">
      <div class="cs-slider_container" data-autoplay="0" data-loop="1" data-speed="600" data-fade-slide="0" data-slides-per-view="responsive" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
        <div class="cs-slider_wrapper">
          <div class="cs-slide">
            <div class="cs-post cs-style1">
              <div class="cs-post_thumb cs-modal_btn" data-modal="details">
                <div class="cs-post_thumb_in cs-bg" data-src="{{asset('public/assets/img/post_1.jpeg')}}"></div>
              </div>
              <div class="cs-post_info">
                <ul class="cs-post_meta cs-mp0">
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16c-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4c14.6 0 38.3 16 89.6 16c51.7 0 74.9-16 89.6-16c47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0S80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96s-96-43.1-96-96s43.1-96 96-96z" />
                      </svg>
                    </span>
                    admin
                  </li>
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M152 64h144V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40h40c35.3 0 64 28.65 64 64v320c0 35.3-28.7 64-64 64H64c-35.35 0-64-28.7-64-64V128c0-35.35 28.65-64 64-64h40V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40zM48 448c0 8.8 7.16 16 16 16h320c8.8 0 16-7.2 16-16V192H48v256z" />
                      </svg>
                    </span>
                    12.09.2022
                  </li>
                </ul>
                <h2 class="cs-post_title cs-modal_btn" data-modal="details">Point of sale software card usde</h2>
                <span class="cs-text_btn cs-modal_btn" data-modal="details">
                  <span>Read More</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M13.3 17.275q-.3-.3-.288-.725q.013-.425.313-.725L16.15 13H5q-.425 0-.713-.288Q4 12.425 4 12t.287-.713Q4.575 11 5 11h11.15L13.3 8.15q-.3-.3-.3-.713q0-.412.3-.712t.713-.3q.412 0 .712.3L19.3 11.3q.15.15.213.325q.062.175.062.375t-.062.375q-.063.175-.213.325l-4.6 4.6q-.275.275-.687.275q-.413 0-.713-.3Z" />
                  </svg>
                </span>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-post cs-style1">
              <div class="cs-post_thumb cs-modal_btn" data-modal="details">
                <div class="cs-post_thumb_in cs-bg" data-src="{{asset('public/assets/img/post_2.jpeg')}}"></div>
              </div>
              <div class="cs-post_info">
                <ul class="cs-post_meta cs-mp0">
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16c-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4c14.6 0 38.3 16 89.6 16c51.7 0 74.9-16 89.6-16c47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0S80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96s-96-43.1-96-96s43.1-96 96-96z" />
                      </svg>
                    </span>
                    admin
                  </li>
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M152 64h144V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40h40c35.3 0 64 28.65 64 64v320c0 35.3-28.7 64-64 64H64c-35.35 0-64-28.7-64-64V128c0-35.35 28.65-64 64-64h40V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40zM48 448c0 8.8 7.16 16 16 16h320c8.8 0 16-7.2 16-16V192H48v256z" />
                      </svg>
                    </span>
                    12.09.2022
                  </li>
                </ul>
                <h2 class="cs-post_title cs-modal_btn" data-modal="details">Best super shop point of sale setup</h2>
                <span class="cs-text_btn cs-modal_btn" data-modal="details">
                  <span>Read More</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M13.3 17.275q-.3-.3-.288-.725q.013-.425.313-.725L16.15 13H5q-.425 0-.713-.288Q4 12.425 4 12t.287-.713Q4.575 11 5 11h11.15L13.3 8.15q-.3-.3-.3-.713q0-.412.3-.712t.713-.3q.412 0 .712.3L19.3 11.3q.15.15.213.325q.062.175.062.375t-.062.375q-.063.175-.213.325l-4.6 4.6q-.275.275-.687.275q-.413 0-.713-.3Z" />
                  </svg>
                </span>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-post cs-style1">
              <div class="cs-post_thumb cs-modal_btn" data-modal="details">
                <div class="cs-post_thumb_in cs-bg" data-src="{{asset('public/assets/img/post_3.jpeg')}}"></div>
              </div>
              <div class="cs-post_info">
                <ul class="cs-post_meta cs-mp0">
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16c-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4c14.6 0 38.3 16 89.6 16c51.7 0 74.9-16 89.6-16c47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0S80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96s-96-43.1-96-96s43.1-96 96-96z" />
                      </svg>
                    </span>
                    admin
                  </li>
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M152 64h144V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40h40c35.3 0 64 28.65 64 64v320c0 35.3-28.7 64-64 64H64c-35.35 0-64-28.7-64-64V128c0-35.35 28.65-64 64-64h40V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40zM48 448c0 8.8 7.16 16 16 16h320c8.8 0 16-7.2 16-16V192H48v256z" />
                      </svg>
                    </span>
                    12.09.2022
                  </li>
                </ul>
                <h2 class="cs-post_title cs-modal_btn" data-modal="details">Computer new point of saie working good</h2>
                <span class="cs-text_btn cs-modal_btn" data-modal="details">
                  <span>Read More</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M13.3 17.275q-.3-.3-.288-.725q.013-.425.313-.725L16.15 13H5q-.425 0-.713-.288Q4 12.425 4 12t.287-.713Q4.575 11 5 11h11.15L13.3 8.15q-.3-.3-.3-.713q0-.412.3-.712t.713-.3q.412 0 .712.3L19.3 11.3q.15.15.213.325q.062.175.062.375t-.062.375q-.063.175-.213.325l-4.6 4.6q-.275.275-.687.275q-.413 0-.713-.3Z" />
                  </svg>
                </span>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-post cs-style1">
              <div class="cs-post_thumb cs-modal_btn" data-modal="details">
                <div class="cs-post_thumb_in cs-bg" data-src="{{asset('public/assets/img/post_4.jpeg')}}"></div>
              </div>
              <div class="cs-post_info">
                <ul class="cs-post_meta cs-mp0">
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16c-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4c14.6 0 38.3 16 89.6 16c51.7 0 74.9-16 89.6-16c47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0S80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96s-96-43.1-96-96s43.1-96 96-96z" />
                      </svg>
                    </span>
                    admin
                  </li>
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M152 64h144V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40h40c35.3 0 64 28.65 64 64v320c0 35.3-28.7 64-64 64H64c-35.35 0-64-28.7-64-64V128c0-35.35 28.65-64 64-64h40V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40zM48 448c0 8.8 7.16 16 16 16h320c8.8 0 16-7.2 16-16V192H48v256z" />
                      </svg>
                    </span>
                    12.09.2022
                  </li>
                </ul>
                <h2 class="cs-post_title cs-modal_btn" data-modal="details">Connection pos to phone payment option</h2>
                <span class="cs-text_btn cs-modal_btn" data-modal="details">
                  <span>Read More</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M13.3 17.275q-.3-.3-.288-.725q.013-.425.313-.725L16.15 13H5q-.425 0-.713-.288Q4 12.425 4 12t.287-.713Q4.575 11 5 11h11.15L13.3 8.15q-.3-.3-.3-.713q0-.412.3-.712t.713-.3q.412 0 .712.3L19.3 11.3q.15.15.213.325q.062.175.062.375t-.062.375q-.063.175-.213.325l-4.6 4.6q-.275.275-.687.275q-.413 0-.713-.3Z" />
                  </svg>
                </span>
              </div>
            </div>
          </div><!-- .cs-slide -->
          <div class="cs-slide">
            <div class="cs-post cs-style1">
              <div class="cs-post_thumb cs-modal_btn" data-modal="details">
                <div class="cs-post_thumb_in cs-bg" data-src="{{asset('public/assets/img/post_2.jpeg')}}"></div>
              </div>
              <div class="cs-post_info">
                <ul class="cs-post_meta cs-mp0">
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16c-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4c14.6 0 38.3 16 89.6 16c51.7 0 74.9-16 89.6-16c47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0S80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96s-96-43.1-96-96s43.1-96 96-96z" />
                      </svg>
                    </span>
                    admin
                  </li>
                  <li>
                    <span class="cs-medium">
                      <svg xmlns="http://www.w3.org/2000/svg" width="0.88em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M152 64h144V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40h40c35.3 0 64 28.65 64 64v320c0 35.3-28.7 64-64 64H64c-35.35 0-64-28.7-64-64V128c0-35.35 28.65-64 64-64h40V24c0-13.25 10.7-24 24-24s24 10.75 24 24v40zM48 448c0 8.8 7.16 16 16 16h320c8.8 0 16-7.2 16-16V192H48v256z" />
                      </svg>
                    </span>
                    12.09.2022
                  </li>
                </ul>
                <h2 class="cs-post_title cs-modal_btn" data-modal="details">Best super shop point of sale setup</h2>
                <span class="cs-text_btn cs-modal_btn" data-modal="details">
                  <span>Read More</span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M13.3 17.275q-.3-.3-.288-.725q.013-.425.313-.725L16.15 13H5q-.425 0-.713-.288Q4 12.425 4 12t.287-.713Q4.575 11 5 11h11.15L13.3 8.15q-.3-.3-.3-.713q0-.412.3-.712t.713-.3q.412 0 .712.3L19.3 11.3q.15.15.213.325q.062.175.062.375t-.062.375q-.063.175-.213.325l-4.6 4.6q-.275.275-.687.275q-.413 0-.713-.3Z" />
                  </svg>
                </span>
              </div>
            </div>
          </div><!-- .cs-slide -->
        </div>
      </div><!-- .cs-slider_container -->
      <div class="cs-pagination cs-style1"></div>
    </div><!-- .cs-slider -->
  </div>
  <div class="cs-height_100 cs-height_lg_70"></div>
</section>
<!-- End Post Stores -->

<!-- Start Contact Section -->
<section class="cs-gradient_bg_1" id="contact">
  <div class="cs-height_95 cs-height_lg_70"></div>
  <div class="container">
    <div class="row">
      <div class="col-xl-5 col-lg-8">
        <div class="cs-seciton_heading cs-style1">
          <div class="cs-section_subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">Contract Us</div>
          <div class="cs-height_10 cs-height_lg_10"></div>
          <h3 class="cs-section_title">If you have any quiries, fill free to contact us</h3>
        </div>
        <div class="cs-height_20 cs-height_lg_20"></div>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the Lorem Ipsum is simply dummy text of the printing and typesetting.
        </p>
        <div class="cs-height_15 cs-height_lg_15"></div>
        <div class="cs-iconbox cs-style3">
          <div class="cs-iconbox_icon">
            <img src="{{asset('public/assets/img/icons/contact_icon_1.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_right">
            <h2 class="cs-iconbox_title">Address</h2>
            <div class="cs-iconbox_subtitle">4117 Leroy LaneHarold, KY 41635,</div>
          </div>
        </div>
        <div class="cs-height_30 cs-height_lg_30"></div>
        <div class="cs-iconbox cs-style3">
          <div class="cs-iconbox_icon">
            <img src="{{asset('public/assets/img/icons/contact_icon_2.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_right">
            <h2 class="cs-iconbox_title">Contract Number</h2>
            <div class="cs-iconbox_subtitle">+606-243-4966</div>
          </div>
        </div>
        <div class="cs-height_30 cs-height_lg_30"></div>
        <div class="cs-iconbox cs-style3">
          <div class="cs-iconbox_icon">
            <img src="{{asset('public/assets/img/icons/contact_icon_3.svg')}}" alt="Icon">
          </div>
          <div class="cs-iconbox_right">
            <h2 class="cs-iconbox_title">Email Address</h2>
            <div class="cs-iconbox_subtitle">demo@gmail.com</div>
          </div>
        </div>
      </div>
      <div class="col-xl-6 offset-xl-1">
        <div class="cs-height_40 cs-height_lg_40"></div>
        
        <form class="cs-contact_form" action="{{route('contact.store')}}" method="POST">
        @csrf
          <h2 class="cs-contact_form_title">Please fill up the form</h2>
          <div class="row">
            <div class="col-lg-6">
              <input type="text" class="cs-form_field" name="name" placeholder="Name">
              <div class="cs-height_25 cs-height_lg_25"></div>
            </div>
            <div class="col-lg-6">
              <input type="text" class="cs-form_field" name="email" placeholder="Email address">
              <div class="cs-height_25 cs-height_lg_25"></div>
            </div>
            <div class="col-lg-12">
              <input type="text" class="cs-form_field" name="mobile" placeholder="Phone number">
              <div class="cs-height_25 cs-height_lg_25"></div>
            </div>
            <div class="col-lg-12">
              <textarea cols="30" rows="5" class="cs-form_field" name="message" placeholder="Write your massage"></textarea>
              <div class="cs-height_25 cs-height_lg_25"></div>
            </div>
            <div class="col-lg-12">
              <button type="submit" name="submit" class="cs-btn cs-size_md"><span>Send Message</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="cs-height_95 cs-height_lg_70"></div>
</section>
<!-- End Contact Section -->

@include ('layout.footer')