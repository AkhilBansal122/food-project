@include('admin.layout.header')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



<script type="text/javascript" src='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js'></script>
<link media="screen" rel="stylesheet" href='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css' />
<link media="screen" rel="stylesheet" href='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.css' />

<div class="page-wrapper">
    <div class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            @include('flash-message')

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Order Summery Details</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="card">
            <div class="card-header custom_col">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xl-2">
                        <input type="Date" class="form-control" name="to_date" id="to_date" />
                    </div>

                    <div class="col-md-4 col-lg-4 col-xl-2">
                        <input type="Date" class="form-control" name="from_date" id="from_date" />
                    </div>


                    <div class="col-md-8 col-lg-8 col-xl-4 d-inline-flex btn_grp">
                        <button type="button" class="btn btn-primary filter me-3"><i class='bx bx-filter-alt'></i>Filter</button>
                        <button type="button" class="btn btn-light refresh me-3 ms-2"><i class='bx bx-refresh'></i></button>
                        <button onclick="export_data(`{{route('restaurent/export_data')}}`)" type="button" class="btn btn-success"><i class="fa fa-file-excel"></i>Export</button>
                    </div>
                </div>

            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table id="orderSummery" class="table table-bordered data-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Transation Id</th>
                                <th>Order Id</th>
                                <th>Customer Name</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>

@include('admin.layout.footer')
<script type="text/javascript">
    $(function() {
        var tables = $('#orderSummery').DataTable({
            processing: false,
            serverSide: true,
            pageLength: 10,
            retrieve: true,
            ajax: {
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                url: "{{ route('restaurent/order_summery_data') }}",
                data: function(d) {
                    d.search = $('input[type="search"]').val(),
                        d.to_date = $('#to_date').val(),
                        d.from_date = $('#from_date').val()
                },

            },



            columns: [{
                    mData: 'id',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    mData: 'payment_id',
                },
                {
                    mData: 'order_id',
                },
                {
                    mData: 'customer_name',
                },
                {
                    mData: 'price'
                }, {
                    mData: 'order_status'
                }, {
                    mData: 'date'
                }
            ]
        });

        $('.refresh').click(function(e) {
            $('#to_date').val("");
            $('#from_date').val("");
            $('.searchBy').val('');
            tables.ajax.reload();
        });
        $('.filter').click(function(e) {
            tables.ajax.reload();
        });
    });


    function export_data(url, id) {
        var to_date = $('#to_date').val();
        var from_date = $('#from_date').val();
        var searchBy = $('.searchBy').val();
        window.location.href = url + '?to_date=' + to_date + '&from_date=' + from_date + '&searchBy=' + searchBy;
    }
</script>