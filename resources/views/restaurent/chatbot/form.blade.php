
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Product Name: <span style="color:red">*</span></label>
        <select class="form-control single-select" name="menu[]" required multiple>
        	<option value="all">All</option>
        	@if(!empty($productlist))
        		@foreach($productlist as $row)
        		<option value="{{$row->name}}">{{ucwords($row->name)}}</option>
        		@endforeach
        	@endif
        </select>
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Customer Name: <span style="color:red">*</span></label>
        <select class="form-control single-select" required multiple name="customer[]" >
        	<option value="all">All</option>
        	@if(!empty($customerlist))
        		@foreach($customerlist as $row)
        		<option value="{{$row->mobile_number}}">{{ucwords($row->firstname)}} ({{$row->mobile_number}})</option>
        		@endforeach
        	@endif
        </select>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
       <label for="passsword" class="form-label">Message: <span style="color:red">*</span> </label>
            <div class="input-group" id="address">
            <textarea name="address" class="form-control mb-3" required style="height:116px"
                placeholder="">{{(isset($data) && !empty($data->address)) ? $data->address:''}}</textarea>
            </div>
            @if($errors->has('address'))<div class="error">{{ $errors->first('address') }}</div>@endif
    </div>
</div>	

<div class="col-12">
    <button type="submit" class="btn btn-light px-5"> {{'Save'}}</button>  
</div>
		