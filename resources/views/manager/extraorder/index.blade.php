@include('admin.layout.header')
<!-- CSS -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.min.js"></script>
</head>

<div class="page-wrapper">
    <div class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Extra Order</div>

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Extra Order</li>
                    </ol>
                </nav>
            </div>
        </div>

        <!--end breadcrumb-->
        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card ">
                    <div class="card-body p-3">
                        @include('flash-message')
                        <form class="row" action="{{route('manager.extra_order_store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @include('manager.extraorder.form')
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered data-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Customer Name</th>
                                <th>Category Mobile</th>
                                <th>Total Amount</th>
                                <th>Discount Amount</th>
                                <th>GST Amount</th>
                                <th>Final Amount</th>
                                <th>Payment Mode</th>
                                <th width="100px">Payment Status</th>
                                <th width="100px">Action</th>

                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>

<div class="modal fade " id="extraOrderDetailsModalshipping">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Extra Order Item Details</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="displayextraorder">



            </div>
        </div>
    </div>
</div>
@include('admin.layout.footer')
<script>
    $(function() {

        $('.single-select').select2({
            theme: 'bootstrap4',
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
        loaddata();
        function loaddata(){

        var table = $('.data-table').DataTable({
            processing: false,
            serverSide: true,
            pageLength: 10,
            retrieve: true,
            ajax: {
                url: "{{ route('manager/extra_order_requestdata') }}",
                data: function(d) {
                    d.search = $('input[type="search"]').val(),
                        d.searchStart_date = $('.searchStart_date').val(),
                        d.searchEndDate = $('.searchEndDate').val()
                },
            },
            columns: [{
                    mData: 'id',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    mData: 'name',
                    name: "name"
                },
                {
                    mData: 'mobile'
                },
                {
                    mData: 'total_amount'
                },
                {
                    mData: 'discount_amount'
                },
                {
                    mData: 'gst_amount'
                },
                {
                    mData: 'final_amount'
                },
                {
                    mData: 'payment_mode'
                },
                {
                    mData: 'status'
                },
                {
                    mData: 'actions'
                },

            ]
        });
    }




    $(document).on("change", "#category_id", function(event) {
        event.preventDefault();
        var category_id = $(this).val();

        $.ajax({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('manager.get_sub_menu')}}",
            data: {
                'category_id': category_id
            },
            datatype: "JSON",
            cache: false,
            dataType: "JSON",
            success: function(response) {
                var htmls = "<option value=''>Select Sub Menu</optio>";
                if (response.status == true) {
                    $("#sub_category_id").empty("");
                    var responseData = response.data;
                    $(responseData).each(function(item, val) {
                        var id = val.id;
                        htmls += "<option data-price=" + val.price + " value=" + id + ">" + val.name + "</option>";
                    });

                    $("#sub_category_id").append(htmls);
                }
            }
        });
    });

    $(document).on("change", "#sub_category_id", function() {
        $("#price").val($(this).find(":selected").attr("data-price"));
    });

    $(document).on("keyup", "#quantity", function() {
        var qty = $(this).val();
        var price = $("#price").val();
        var total = qty * price;
        $("#total_price").val(total);
    });



    $(document).on("click", ".view_extra_order", function() {
        var customers_id = $(this).data("id");
        route = "{{route('manager/view_extra_order')}}";
        jQuery.ajax({
            url: route,
            type: "post",
            cache: false,
            data: {
                customers_id: customers_id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            beforeSend: function(msg) {},
            success: function(data) {
                $("#extraOrderDetailsModalshipping").modal('show');
                 $("#displayextraorder").html(data);
            }
        });
    });
    
    $(document).on("change", ".select_changes3", function() {
        var customers_id = $(this).data("id");
        var payment_status = $(this).data('payment_status');
        route = "{{route('manager/extra_status_change')}}";
        jQuery.ajax({
            url: route,
            type: "post",
            cache: false,
            data: {
                customers_id: customers_id,
                payment_status:payment_status
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            beforeSend: function(msg) {},
            success: function(data) {
                loaddata();
      
            }
        });
    });
});
</script>