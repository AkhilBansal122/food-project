<style>
    @media print {

        /* Hide non-essential elements when printing */
        body * {
            visibility: hidden;
        }

        .print-container,
        .print-container * {
            visibility: visible;
        }

        .print-container {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
        }
    }
</style>
<div class="row mb-4">
    <div class="col text-center">
        <h1>Invoice</h1>
    </div>
</div>
<div class="container print-container" style="border: 1px solid lightgray;">
    <div class="row text-center">
        <div class="col-7">
            <address>

                <p>Branch Name : {{$data[0]['customerDetails']['branchDetails']['name']}}</p>
                <p>Branch No : {{$data[0]['customerDetails']['branchDetails']['contact1']}}</p>
                <address>

        </div>
        <div class="col-5">
            <p>Extra Order Id: EXTODR000{{$data[0]['customerDetails']['id']}}</h5>
            <p>Order Date: {{$data[0]['customerDetails']['created_at']->format('d-M-Y')}}</p>
        </div>
        <style type="text/css">
            
        </style>
        <div class="mb-4 col-12 col-md-12 col-sm-12" style="border:1px solid lightgray;">
            <div class="" >
                <h5>Billing Address:</h5>
                <address>
                    Name:{{$data[0]['customerDetails']['name']}}<br>
                    Mobile:{{$data[0]['customerDetails']['mobile']}}<br> 

                </address>
            </div>

        </div>
        <table class="table table-bordered">
            <thead style="color:black;">
                <tr>
                    <th>S.N.</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($data))
                @foreach($data as $i=> $row)
                <tr>
                    <td>{{$i+1}}</td>
                    <td>{{$row->categoryDetails->name}}</td>
                    <td>{{$row->subDetails->name}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->price}}</td>
                    <td>{{$row->total_price}}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5" class="text-end">Total Amount:</th>
                    <th>{{$data[0]['customerDetails']['total_amount']}}</th>
                </tr>
                <tr>
                    <th colspan="5" class="text-end">Discount Amount:</th>
                    <th>{{$data[0]['customerDetails']['discount_amount']}}</th>
                </tr>
                <tr>
                    <th colspan="5" class="text-end">Gst Amount:</th>
                    <th>{{$data[0]['customerDetails']['gst_amount']}}</th>
                </tr>
                <tr>
                    <th colspan="5" class="text-end">Grand Total:</th>
                    <th>{{$data[0]['customerDetails']['final_amount']}}</th>
                </tr>
            </tfoot>
        </table>
        <style type="text/css">
            hr {
                display: block;
            }
        </style>
        <hr>
        <div class="row">
            <div class="col text-center">
                <p>Thank you for your business!</p>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col text-center mb-3">
            <button class="btn text-white" style="background-color:orange;" onclick="window.print()"><b>Print</b></button>
        </div>
    </div>

</div>