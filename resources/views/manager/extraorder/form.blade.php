
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Enter Full Name: <span style="color:red">*</span></label>
        <input type="text" 
        name="name"
        aria-required="true"
        class="form-control"  placeholder="Please Enter Name" required >
        @if($errors->has('name'))<div class="error">{{ $errors->first('name') }}</div>@endif
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Enter Mobile Number: <span style="color:red">*</span></label>
        <input type="text" 
        name="mobile"
        min="10"
        max="10"
        aria-required="true"
        id="mobile"
        class="form-control"  placeholder="Please enter mobile" required >
        @if($errors->has('mobile'))<div class="error">{{ $errors->first('mobile') }}</div>@endif
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Select Category: <span style="color:red">*</span></label>
        <select name="category_id" id="category_id" class="form-control single-select"         aria-required="true">
            <option value="">Select Category</option>
            @if(!empty($category))
                @foreach($category as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label for="title" class="form-label">Select Menu: <span style="color:red">*</span></label>
        <select name="sub_category_id" id="sub_category_id" class="form-control single-select"         aria-required="true">
            <option value="">Select Menu</option>
        </select>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Enter Quantity: <span style="color:red">*</span></label>
        <input type="number" min="0"
        aria-required="true"
        name="qty"
        value="" 
        min="1"
        id="quantity"
        class="form-control"  placeholder="Please enter quantity" required >
        @if($errors->has('qty'))<div class="error">{{ $errors->first('qty') }}</div>@endif
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Product Price: <span style="color:red">*</span></label>
        <input type="number" readonly id="price" 
        aria-required="true"
        name="price" value="0"
        class="form-control"  placeholder="" required >
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <label for="title" class="form-label">Total Price: <span style="color:red">*</span></label>
        <input type="number" readonly id="total_price" name="total_price" value="0"
        class="form-control"  placeholder="" required >
    </div>
</div>

<div class="col-12">
    <button type="submit" class="btn btn-light px-5"> {{isset($data->id) ? 'Update' :'Save' }}</button>
</div>	