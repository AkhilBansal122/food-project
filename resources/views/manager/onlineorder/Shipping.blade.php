<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Bill Print</title>
    <style>
        @media print {

            /* Hide non-essential elements when printing */
            body * {
                visibility: hidden;
            }

            .print-container,
            .print-container * {
                visibility: visible;
            }

            .print-container {
                position: fixed;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
            }
        }
    </style>
</head>

<body>
    <div class="row mb-4">
        <div class="col text-center">
            <h1>Invoice</h1>
        </div>
    </div>
    <div class="container print-container" style="border: 1px solid lightgray;">

        <div class="row text-center">
            <!-- <div class="col-3">
                                <img src="{{asset('assets/website/hero.png')}}">
                            </div> -->
            <div class="col-7">
                <h5>{{$getShipping->branchDetails->name}}</h5>
                <p>{{$getShipping->branchDetails->address}}</p>
            </div>
            <div class="col-5">
                <p>Order Id: {{$getShipping->orderDetails->unique_id}}</h5>
                <p>Order Date: {{$getShipping->orderDetails->created_at->format('d-M-Y')}}</p>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-6 col-md-6 col-sm-6" style="border:1px solid lightgray;">
                <h5>Billing Address:</h5>
                <address>
                    Name:{{$getShipping->userDetails->name}}<br>
                    {{$getShipping->userDetails->permanent_address}}<br>
                    {{$getShipping->userDetails->local_address}}<br>
                    {{$getShipping->userDetails->mobile_number}}
                </address>
            </div>
            <div class="col-6 col-md-6 col-sm-6" style="border:1px solid lightgray;">
                <h5>Shipping Address:</h5>
                <address>
                    {{$getShipping->first_name ." ". $getShipping->first_name}}<br>
                    {{$getShipping->phone}}<br>
                    {{$getShipping->address}}<br>
                    {{$getShipping->city}}<br>
                    {{$getShipping->state}}<br>
                    {{$getShipping->postcode}}<br>

                </address>
            </div>
        </div>
        <table class="table table-bordered">
            <thead style="color:black;">
                <tr>
                    <th>S.N.</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($getShipping->order_item as $row)
                <tr>
                    <td>1</td>
                    <td>{{$row->productDetails->name}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->product_price}}</td>
                    <td>{{$row->qty * $row->product_price}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4" class="text-end">Grand Total:</th>
                    <th>{{$getShipping->orderDetails->final_amount}}</th>
                </tr>
            </tfoot>
        </table>
        <style type="text/css">
            hr {
                display: block;
            }
        </style>
        <hr>
        <div class="row">
            <div class="col text-center">
                <p>Thank you for your business!</p>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col text-center">
            <button class="btn text-white" style="background-color:orange;" onclick="window.print()"><b>Print</b></button>
            <a href="{{url('/manager/generate-pdf')}}" class="btn text-white" style="background-color:orange;" ><b>Share</b></a>
        </div>
    </div>
</body>
</html>