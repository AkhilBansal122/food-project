@include('admin.layout.header')
<!-- CSS -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.min.js"></script>
</head>

<div class="page-wrapper">
    <div class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Online Order</div>

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Online Order</li>
                    </ol>
                </nav>
            </div>
        </div>

        <!--end breadcrumb-->

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered data-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Order Id</th>
                                <th>Customer Name</th>
                                <th>Price</th>
                                <th>Total Price</th>
                                <th>Payment Mode</th>
                                <th width="100px">Status</th>
                                <th width="100px">Order Status</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
<div class="modal fade " id="onlineOrderDetailsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Order Item Details</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="online_viewOrder" class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Order Id</th>
                                        <th>Product Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th width="100px">Status</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="onlineOrderDetailsModalshipping">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Order Item Details</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="displayshipping">
            </div>
        </div>
    </div>
</div>

@include('admin.layout.footer')
<script>
    $(function() {
        var table = $('#example').DataTable({
            processing: false,
            serverSide: true,
            searching: false,
            pageLength: 10,
            retrieve: true,
            ajax: {
                url: "{{ route('manager/online_order_requestdata') }}",
                data: function(d) {
                    d.search = $('input[type="search"]').val(),
                        d.searchStart_date = $('.searchStart_date').val(),
                        d.searchEndDate = $('.searchEndDate').val()
                },
            },
            columns: [{
                    mData: 'id',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    mData: 'unique_id'
                },
                {
                    mData: 'customer_name',
                    name: "customer_name"
                },
                {
                    mData: 'price',
                    name: 'price'
                },
                {
                    mData: 'total_price',
                    name: 'total_price'
                },
                {
                    mData: 'payment_mode'
                },
                {
                    mData: 'order_status',
                    name: "order_status"
                },
                {
                    mData: 'action',
                    name: "action"
                },
                {
                    mData: 'actions',
                    name: "actions"
                },

            ]
        });



    });

    $(document).on("click", ".view_online_order", function() {
        var id = $(this).data("id");
        $('#onlineOrderDetailsModal').modal('show');
        route = "{{route('manager/view_online_order')}}";
        //  alert(route);
        var table = $('#online_viewOrder').DataTable({
            processing: false,
            serverSide: true,
            pageLength: 10,
            searching: false,
            retrieve: true,
            ajax: {
                url: route,
                data: function(d) {
                    d.order_id = id
                },
            },
            columns: [{
                    mData: 'id',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    mData: 'order_id'
                },
                {
                    mData: 'product_name'
                },
                {
                    mData: 'qty'
                },
                {
                    mData: 'price'
                },
                {
                    mData: 'order_status'
                }
            ]
        });

    });

    $(document).on("click", ".view_shipping_address", function() {
        var id = $(this).data("id");
        $('#onlineOrderDetailsModalshipping').modal('show');
        route = "{{route('manager/view_shipping_address')}}";
        jQuery.ajax({
            url: route,
            type: "post",
            cache: false,
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            beforeSend: function(msg) {},
            success: function(data) {
                $("#displayshipping").html(data);
            }
        });
    });

    function select_changes3(id, selectoption) {
        //$(selectoption).find(':selected').data('id');
        value = selectoption;
        formData = {
            id: id,
            order_in_process: value
        };
        route = "{{ route('manager/online_status_change') }}";
        ajaxCall(route, formData)
    }
</script>