@include('website.layout.header')
<?php
 $directoryURI = $_SERVER['REQUEST_URI'];
 $path = parse_url($directoryURI, PHP_URL_PATH);
 $components = explode('/', $path);

 $uri1= isset($components[2]) ? $components[2] :'';

 $uri2 = isset($components[3]) ? $components[3] :'';
 $checked = false;

 if(substr($uri1, 0, 4)==="TBL-")
 {
    $checked = true;    
 }else{
   $restaurentName = $uri1;
 }
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<script src="https://checkout.razorpay.com/v1/checkout.js" ></script>
<script
   src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script>

<input type="hidden" id="table_id" value="{{$id}}" />
<style>
   .razorpay-payment-button{
      display: none;
   }
   </style>
<div class="container-xxl py-5 bg-dark hero-header mb-5">
   <div class="container text-center my-5 pt-5 pb-4">
      <h1 class="display-3 text-white mb-3 animated slideInDown">Food Menu</h1>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb justify-content-center text-uppercase">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Pages</a></li>
            <li class="breadcrumb-item text-white active" aria-current="page">Menu</li>
         </ol>
      </nav>
   </div>
</div>
</div>
<!-- Navbar & Hero End -->


<div class="container-xxl py-5">
   <div class="container">
      <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
         <h5 class="section-title ff-secondary text-center text-primary fw-normal">Food Menu</h5>
         <h1 class="mb-5">Most Popular Items</h1>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-8">
               <div class="row mt-4">
                  <div class="col-12">
                     <nav aria-label="breadcrumb">
                        <div class="container bttn">
                           @if(!empty($getMenu))
                           <input type="hidden" id="menu_id" value="{{$getMenu[0]['id']}}" />
                           @foreach($getMenu as $k=> $row)
                           <button onClick="menuSelect(`{{$row->id}},{{$k}}`);return false" class="addtocard">
                              <span>
                                 Popular
                                 <p>{{$row->name}}</p>
                              </span>
                           </button>
                           @endforeach
                           @endif
                        </div>
                        <style type="text/css">
                           .btn-outline-secondary:hover {
                              background-color: orange;
                              border: none;
                           }
                        </style>
                        <div class="container mt-5">
                           <div class="row">
                              <div class="tab-content" id="sub_menu_div"> </div>
                           </div>
                        </div>
                     </nav>
                  </div>
               </div>
            </div>
            <div class="col-4">
               <div class="container">
                  <div class="row">
                     <div class="col-12">
                        <h4 class="mt-3">My orders</h4>
                        <hr>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12" id="cartitemDiv"></div>
                  </div>
                  <div class="d-flex justify-content-between align-items-center mt-3">
                     <div class="btn-group">
                        <p>Sub Total</p>
                     </div>
                     <small class="text-body-secondary" id="sub_total">Rs.0.00</small>
                  </div>
                  <div class="d-flex justify-content-between align-items-center">
                     <div class="btn-group">
                        <p>Discount Amount</p>
                     </div>
                     <small class="text-body-secondary" id="discount_amount">Rs.0.00</small>
                  </div>
                  <div class="d-flex justify-content-between align-items-center">
                     <div class="btn-group">
                        <p>Delivery Charge</p>
                     </div>
                     <small class="text-body-secondary" id="shipping_amount">Rs.0.00</small>
                  </div>
                  <div class="d-flex justify-content-between align-items-center">
                     <div class="btn-group">
                        <p>GST Amount</p>
                     </div>
                     <small class="text-body-secondary" id="gstAmount">Rs 0.00</small>
                  </div>
                  <hr>
                  <div class="d-flex justify-content-between align-items-center">
                     <div class="btn-group">
                        <p>Total </p>
                     </div>
                     <small class="text-body-secondary" id="total_final_amount">Rs. 0.00</small>
                  </div>
                  @if($checked== true)
                  <form action="{{ route('Phonepe') }}" method="POST" id="orderForm">
                       @csrf
          
                     <input type="hidden" name="sub_total_hidden" id="sub_total_hidden" name=""  value=""/>
                     <input type="hidden" name="id_hidden" id="id_hidden"  value=""/>
                     <input type="hidden" name="amount" id="total_hidden"  value=""/>
                     <input id="table_id" name ="table_id" type="hidden" value="{{auth()->user()->table_id ?? $restaurentName}}"/>
                    
                     <button type="submit"   class="form-control text-white" style="border: none; background-color: orange;" >Check Out</button>
                  </form>
                     @else 
                  <input type="hidden" name="total_hidden" id="total_hidden2"  value=""/>
                  
                  <button  class="form-control text-white" onclick="showModal();" style="border: none; background-color: orange;" >Check Out</button>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<link rel="stylesheet" href="{{ asset('public/assets/website/css/core-style.css')}}">


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content container" style="width: 230%; margin-left: -310px;">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
            @if($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button>
               <strong>Error!</strong> {{ $message }}
            </div>
            @endif

            @if($message = Session::get('success'))
            <div class="alert alert-info alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button>
               <strong>Success!</strong> {{ $message }}
            </div>
            @endif
            <form action="{{ route('Phonepe') }}" method="POST" id="orderForm">
               @csrf
             
               <input type="hidden" name="_token" value="{{csrf_token()}}"/>
               
               <div class="row">
                  <div class="col-12 col-md-6">
                     <div class="checkout_details_area clearfix">
                        <div class="cart-page-heading">
                           <h5 style="color: #fea116;">Shipping Address</h5>
                        </div>
                        <div class="row">
                           <div class="col-12 col-md-6 mb-2 px-0">
                              <label for="first_name">First Name <span>*</span></label>
                              <input type="text" class="form-control" aria-required="true" required placeholder="Enter first name"  name="first_name" id="shipping_first_name" value="" required="true">
                           </div>
                           <div class="col-12 col-md-6 mb-2 px-0">
                              <label for="last_name">Last Name <span>*</span></label>
                              <input type="text" class="form-control" aria-required="true" required placeholder="Enter last name" name="last_name" id="shipping_last_name" value="" required="true">
                           </div>
                           <div class="col-12 mb-2 px-0">
                              <label for="email">Email <span>*</span></label>
                              <input type="email" class="form-control" aria-required="true"  required="" placeholder="Enter Email" name="email" id="shipping_email" value="" required="true">
                           </div>
                           <div class="col-12 mb-2 px-0 ">
                              <label for="street_address">Phone <span>*</span></label>
                              <input type="text" class="form-control" placeholder="Phone" name="phone" id="shipping_phone" value="" required="">
                           </div>
                           <div class="col-12 mb-2 px-0 ">
                              <label for="street_address">Address <span>*</span></label>
                              <input type="text" class="form-control" aria-required="true" required placeholder="Address" name="address_one" id="shipping_address_one" value="" required="">
                           </div>
                           <div class="col-6 mb-2 px-0">
                              <label for="city">City <span>*</span></label>
                              <input type="text" class="form-control" aria-required="true" required placeholder="Enter city" name="city" id="shipping_city" value="" required="true">
                           </div>
                           <div class="col-6 mb-2 px-0">
                              <label for="state">State <span>*</span></label>
                              <input type="text" class="form-control" aria-required="true" required placeholder="Enter state" name="state" id="shipping_state" value="" required="true">
                              <input type="hidden" name="country" value="India">
                           </div>
                           <div class="col-12 mb-2 px-0 ">
                              <label for="postcode">Postcode <span>*</span></label>
                              <input type="text" maxlength="6" aria-required="true" required minlength="6" class="form-control" placeholder="zip code" name="zip" id="shipping_zip" value="" required="">
                              <div class="text-left p-2 mt-1 zip-message"></div>
                           </div>
                        </div>
                        <input id="showFinalpaymentField" type="hidden" name="amount" value="" />
                        <input id="table_id" name ="table_id" type="hidden" value="{{auth()->user()->table_id ?? $restaurentName}}"/>
                        <div class="checkout_detail_area clearfix" id="billing_area" style="display: none">
                           <div class="cart-page-heading mt-30"></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-6 ml-lg-auto">
                     <div class="order-details-confirmation">
                        <div class="cart-page-heading">
                           <h5 style="color: #fea116;">Your Order</h5>
                           <p>Payment Details</p>
                           <span id="showFinalpayment">12</span>
                        </div>
                        <div class="row col-12 p-0 m-0">
                           <button type="submit" class="btn karl-checkout-btn mt-3 text-white"  style="background-color: #fea116;">Place Order</button>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>

      </div>
   </div>
</div>

<!-- <div class="modal fade" id="exampleModalTab" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"> -->
<div class="modal fade" id="exampleModalTab" tabindex="-1" data-target=".bd-example-modal-sm" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"> 
<div class="modal-dialog">
      <div class="modal-content container" style="width: 230%; margin-left: -310px;">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
            @if($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button>
               <strong>Error!</strong> {{ $message }}
            </div>
            @endif

            @if($message = Session::get('success'))
            <div class="alert alert-info alert-dismissible fade in" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
               </button>
               <strong>Success!</strong> {{ $message }}
            </div>
            @endif
         
               <input type="hidden" name="_token" value="{{csrf_token()}}"/>
               <div class="row">
                  <div class="col-12 col-md-6">
                     <div class="checkout_details_area clearfix">
                        <nav>
                           <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
                              <button class="nav-link active" id="login-tab" data-bs-toggle="tab" data-bs-target="#login" type="button" role="tab" aria-controls="login" aria-selected="true">Login</button>
	                  			<button class="nav-link" id="register-tab" data-bs-toggle="tab" data-bs-target="#register" type="button" role="tab" aria-controls="register" aria-selected="false">Register</button>
	                        </div>
                        </nav>
                        <div class="tab-content p-3 border bg-light" id="nav-tabContent">
                           <div class="tab-pane fade active show" id="login" role="tabpanel" aria-labelledby="login-tab">
                              <div class="row">
                              <form action="javascript:void(0)" method="POST" id="loginForm">
                                 @csrf
                                 <div class="col-12 mb-2 px-0">
                                    <label for="email">Email <span>*</span></label>
                                    <input type="email" class="form-control" placeholder="Enter Email" name="email" id="loginemail" value="" required="true">
                                 </div>
                                 <div class="col-12 mb-2 px-0 ">
                                    <label for="street_address">Password <span>*</span></label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="loginpassword" value="" required="">
                                 </div>
                                 <button type="submit" class="btn karl-checkout-btn mt-3 text-white" id="loginform-submit" style="background-color: #fea116;">Login</button>
                                 </form>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                           <div class="row">
                           <form action="javascript:void(0)"  method="POST" id="registerForm">
                                 @csrf
                                 <div class="col-12 col-md-6 mb-2 px-0">
                                    <label for="first_name">First Name <span>*</span></label>
                                    <input type="text" class="form-control" aria-required="true" placeholder="Enter first name" name="first_name" id="regfirst_name" value="" required="true">
                                 </div>
                                 <div class="col-12 col-md-6 mb-2 px-0">
                                    <label for="last_name">Last Name <span>*</span></label>
                                    <input type="text" class="form-control" aria-required="true" placeholder="Enter last name" name="last_name" id="reglast_name" value="" required="true">
                                 </div>
                                 <div class="col-12 mb-2 px-0">
                                    <label for="email">Email <span>*</span></label>
                                    <input type="email" class="form-control" aria-required="true" placeholder="Enter Email" name="email" id="regemail" value="" required="true">
                                 </div>
                                 <div class="col-12 mb-2 px-0 ">
                                    <label for="street_address">Phone <span>*</span></label>
                                    <input type="text" class="form-control" aria-required="true" placeholder="Phone" name="mobile_number" id="regphone" value="" required="">
                                 </div>
                                 <div class="col-12 mb-2 px-0 ">
                                    <label for="street_address">Password <span>*</span></label>
                                    <input type="password" class="form-control" aria-required="true" placeholder="Password" name="password" id="regpassword" value="" required="">
                                 </div>
                                 <div class="col-12 mb-2 px-0 ">
                                    <label for="street_address">Select Branch <span>*</span></label>
                                    <select id="branch_id" class="form-control" required>
                                    <option value="">Select Branch</option>
                                    @foreach($getBranch as $row)
                                       <option value="{{$row->id}}">{{$row->name}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                                 
                                 <button type="submit" class="btn karl-checkout-btn mt-3 text-white" id="registerform-submit" style="background-color: #fea116;">Submit</button>
                                 </form>
                              </div>
                        </div>
                     </div>
                  </div>
               </div>
            
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script>

$("body").on("click","#registerform-submit",function(e){
   e.preventDefault();
   var first_name = $("#regfirst_name").val();
   var last_name = $("#reglast_name").val();
   var email = $("#regemail").val();
   var password = $("#regpassword").val();
   var phone = $("#regphone").val();
   var branch_id = $("#branch_id").val();
   if(first_name=='' || first_name==null || first_name==undefined)
   {
      toastr.error("Please Enter First Name");
      return false;
   }
   if(last_name=='' || last_name==null || last_name==undefined)
   {
      toastr.error("Please Enter Last Name");
      return false;
   }
   if(email=='' || email==null || email==undefined)
   {
      toastr.error("Please Enter Email");
      return false;
   }
   if(phone=='' || phone==null || phone==undefined)
   {
      toastr.error("Please Enter Phone Number");
      return false;
   }

   if(password=='' || password==null || password==undefined)
   {
      toastr.error("Please Enter Password");
      return false;
   }
   if(branch_id=='' || branch_id==null || branch_id==undefined)
   {
      toastr.error("Please Select Branch");
      return false;
   }
   
   if(first_name!='' && last_name!='' && email!='' && phone!='' && password !=''){
      data={
         'restaurentName':`{{$restaurentName}}`,
         "first_name":first_name,
         "last_name":last_name,
         "email":email,
         "mobile_number":phone,
         "password":password,
         "branch_id":branch_id
      };
      $.ajax({
         url: "{{route('customerSignUp')}}",
         data: data,
            method: 'POST',
            type: "post",
            cache: false,
            dataType: 'JSON',
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
               $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
            },
            success: function(response) {
               $("#spinner").removeClass("show");
               if (response.status == true) {
                  toastr.success(response.message);
               //   cartItemCAll(table_id);
               $("#exampleModalTab").modal('hide');
               }else if(response.type=='1') {

                  $("#exampleModalTab").modal('show');
                  toastr.error(response.message);
               }
               else{
                  toastr.error(response.message);
               }
            },
            error: function(response) {}
         });
   }
});

$("body").on("click","#loginform-submit",function(e){
   e.preventDefault();
   var email = $("#loginemail").val();
   var password = $("#loginpassword").val();
  
   if(email=='' || email==null || email==undefined)
   {
      toastr.error("Please Enter Email");
      return false;
   }

   if(password=='' || password==null || password==undefined)
   {
      toastr.error("Please Enter Password");
      return false;
   }
   
   if(email!='' &&  password !=''){
      data={
         'restaurentName':`{{$restaurentName}}`,
         "email":email,
         "password":password,
      };
      $.ajax({
         url: "{{route('customerLogin')}}",
         data: data,
            method: 'POST',
            type: "post",
            cache: false,
            dataType: 'JSON',
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
               $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
            },
            success: function(response) {
               $("#spinner").removeClass("show");
               if (response.status == true) {
                  toastr.success(response.message);
               //   cartItemCAll(table_id);
               $("#exampleModalTab").modal('hide');
               }else if(response.type=='1') {

                  $("#exampleModalTab").modal('show');
                  toastr.error(response.message);
               }
               else{
                  toastr.error(response.message);
               }
            },
            error: function(response) {}
         });
   }
});

$('body').on('click','#rzp-button1',function(e){

    e.preventDefault();

    var amount = $('#total_hidden').val();

    var total_amount = amount * 100;

    var options = {

        "key": "{{ env('RAZORPAY_KEY') }}", // Enter the Key ID generated from the Dashboard

        "amount": total_amount, // Amount is in currency subunits. Default currency is INR. Hence, 10 refers to 1000 paise

        "currency": "INR",

        "name": "NiceSnippets",

        "description": "Test Transaction",

        "image": "https://www.nicesnippets.com/image/imgpsh_fullsize.png",

        "order_id": "", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1

        "handler": function (response){

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': `{{csrf_token()}}`

                }

            });

            $.ajax({

                type:'POST',

                url:"{{ route('checkout') }}",

                data:{razorpay_payment_id:response.razorpay_payment_id,amount:amount,table_id:"{{auth()->user()->table_id ?? $restaurentName}}"},

                success:function(data){

                  toastr.success(data.success);
                  

                    $('.amount').val('');

                }

            });

        },

        "prefill": {

            "name": "",

            "email": "",

            "contact": "818********6"

        },

        "notes": {

            "address": "test test"

        },

        "theme": {

            "color": "#F37254"

        }

    };

    var rzp1 = new Razorpay(options);

    rzp1.open();

});
$("body").on("click",'#rzp-button2',function(e){
e.preventDefault();
var amount = $('#total_hidden2').val();
//alert(amount);
if(amount==0 || amount.length==0 || amount=='')
{
   toastr.error("Please Add To Cart Item");
   return false;
}
else{
   var shipping_first_name =$("#shipping_first_name").val();
   var shipping_last_name= $("#shipping_last_name").val();
   var shipping_email = $("#shipping_email").val();

   var shipping_phone = $("#shipping_phone").val();
   var shipping_address_one = $("#shipping_address_one").val();
   var shipping_city=$("#shipping_city").val();
   var shipping_state =$("#shipping_state").val();
   var shipping_zip = $("#shipping_zip").val();
   if(shipping_first_name=='' || shipping_first_name==null || shipping_first_name==undefined)
   {
      toastr.error("Please Enter First Name");
      return false;
   }
   if(shipping_last_name=='' || shipping_last_name==null || shipping_last_name==undefined)
   {
      toastr.error("Please Enter Last Name");
      return false;
   }
   if(shipping_email=='' || shipping_email==null || shipping_email==undefined)
   {
      toastr.error("Please Enter Email");
      return false;
   }
   if(shipping_phone=='' || shipping_phone==null || shipping_phone==undefined)
   {
      toastr.error("Please Enter Phone Number");
      return false;
   }
   if(shipping_address_one=='' || shipping_address_one==undefined)
   {
      toastr.error("Please Enter Address");
      return false;
   }
   if(shipping_city=='' || shipping_city==null || shipping_city==undefined)
   {
      toastr.error("Please Enter City");
      return false;
   }
   if(shipping_zip=='' || shipping_zip==null || shipping_zip==undefined)
   {
      toastr.error("Please Enter Postcode");
      return false;
   }

   if(shipping_first_name!='' && 
      shipping_city!='' && 
      shipping_zip!='' && 
      shipping_state!='' && 
      shipping_last_name!='' && 
      shipping_email!='' && 
      shipping_phone!='' && 
      shipping_address_one!=''){
      var total_amount = amount * 100;

      datashipping ={
         'first_name':shipping_first_name,
         'last_name':shipping_last_name,
         'email':shipping_email,
         'phone':shipping_phone,
         'address':shipping_address_one,
         'city':shipping_city,
         'state':shipping_state,
         'shipping_zip':shipping_zip,
      };

      $.ajaxSetup({

headers: {

    'X-CSRF-TOKEN': `{{csrf_token()}}`

}

});

$.ajax({

   type:'POST',

   url:"{{ route('Phonepe') }}",

   data:{datashipping:datashipping,order_mode:1,amount:amount,table_id:"{{auth()->user()->table_id ?? $restaurentName}}"},

   success:function(data){
   cartItemCAll($("#table_id").val());
      if(data.type==true)
      {
         toastr.success(data.success);
         window.location.reload();
      }else{
         toastr.success(data.success);
      }
      $('.amount').val('');

   }
});
   }
}
});
$('body').on('click','#rzp-button3',function(e){

e.preventDefault();

var amount = $('#total_hidden2').val();
//alert(amount);
if(amount==0 || amount.length==0 || amount=='')
{
   toastr.error("Please Add To Cart Item");
   return false;
}
else{
   var shipping_first_name =$("#shipping_first_name").val();
   var shipping_last_name= $("#shipping_last_name").val();
   var shipping_email = $("#shipping_email").val();

   var shipping_phone = $("#shipping_phone").val();
   var shipping_address_one = $("#shipping_address_one").val();
   var shipping_city=$("#shipping_city").val();
   var shipping_state =$("#shipping_state").val();
   var shipping_zip = $("#shipping_zip").val();
   if(shipping_first_name=='' || shipping_first_name==null || shipping_first_name==undefined)
   {
      toastr.error("Please Enter First Name");
      return false;
   }
   if(shipping_last_name=='' || shipping_last_name==null || shipping_last_name==undefined)
   {
      toastr.error("Please Enter Last Name");
      return false;
   }
   if(shipping_email=='' || shipping_email==null || shipping_email==undefined)
   {
      toastr.error("Please Enter Email");
      return false;
   }
   if(shipping_phone=='' || shipping_phone==null || shipping_phone==undefined)
   {
      toastr.error("Please Enter Phone Number");
      return false;
   }
   if(shipping_address_one=='' || shipping_address_one==undefined)
   {
      toastr.error("Please Enter Address");
      return false;
   }
   if(shipping_city=='' || shipping_city==null || shipping_city==undefined)
   {
      toastr.error("Please Enter City");
      return false;
   }
   if(shipping_zip=='' || shipping_zip==null || shipping_zip==undefined)
   {
      toastr.error("Please Enter Postcode");
      return false;
   }

   if(shipping_first_name!='' && 
      shipping_city!='' && 
      shipping_zip!='' && 
      shipping_state!='' && 
      shipping_last_name!='' && 
      shipping_email!='' && 
      shipping_phone!='' && 
      shipping_address_one!=''){
      var total_amount = amount * 100;

      datashipping ={
         'first_name':shipping_first_name,
         'last_name':shipping_last_name,
         'email':shipping_email,
         'phone':shipping_phone,
         'address':shipping_address_one,
         'city':shipping_city,
         'state':shipping_state,
         'shipping_zip':shipping_zip,
      };
      var options = {

         "key": "{{ env('RAZORPAY_KEY') }}", // Enter the Key ID generated from the Dashboard

         "amount": total_amount, // Amount is in currency subunits. Default currency is INR. Hence, 10 refers to 1000 paise

         "currency": "INR",

         "name": "NiceSnippets",

         "description": "Test Transaction",

         "image": "https://www.nicesnippets.com/image/imgpsh_fullsize.png",

         "order_id": "", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1

         "handler": function (response){

           $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': `{{csrf_token()}}`

            }

           });

           $.ajax({

               type:'POST',

               url:"{{ route('checkout') }}",
          
               data:{datashipping:datashipping,order_mode:1,razorpay_payment_id:response.razorpay_payment_id,amount:amount,table_id:"{{auth()->user()->table_id ?? $restaurentName}}"},

               success:function(data){
               cartItemCAll($("#table_id").val());
                  if(data.type==true)
                  {
                     toastr.success(data.success);
                     window.location.reload();
                  }else{
                     toastr.success(data.success);
                  }
                  $('.amount').val('');

               }
         });
    },

    "prefill": {

        "name": "",

        "email": "",

        "contact": "818********6"

    },

    "notes": {

        "address": "test test"

    },

    "theme": {

        "color": "#F37254"

    }
};
   var rzp1 = new Razorpay(options);
   rzp1.open();
   }
}
});

</script>
<script type="text/javascript">
   //initialising a variable name data

   function showModal(){
      var amount = $('#total_hidden2').val();
      if(amount.length==0)
      {
         toastr.success("Please Add To Cart Item");
                 return false;
      }
      $("#showFinalpaymentField").val(amount);
         $("#showFinalpayment").text("₹."+ $("#total_hidden2").val() + "/-");
         $('#exampleModal').modal('show');

   }
   var data = 0;
   //   $("#spinner").hide();
   //printing default value of data that is 0 in h2 tag
   //  document.getElementById("counting").innerText = data;

   //creation of increment function
   function increment(key, qty) {

      var id = ".quantity_" + key;
      quantity = parseInt($(id).val());

      cart_id = $(id).data('cart_id');
      cart_details_id = $(id).data('cart_item_id');

      quantity += 1;
      table_id = $("#table_id").val();
      //            console.log("cart_id->",cart_details_id);
      $(id).val(quantity);
      data = {
         "type": 1,
         "qty": quantity,
         "cart_id": cart_id,
         "cart_details_id": cart_details_id
      }
      CartItemIncDec(data, table_id);
   }
   //creation of decrement function
   function decrement(key, qty) {
      var id = ".quantity_" + key;

      cart_id = $(id).data('cart_id');
      cart_details_id = $(id).data('cart_item_id');

      table_id = $("#table_id").val();

      quantity = parseInt($(id).val());
      quantity -= 1;
      if (quantity > 0) {
         $(id).val(quantity);
         data = {
            "type": 2,
            "qty": quantity,
            "cart_id": cart_id,
            "cart_details_id": cart_details_id
         };
         CartItemIncDec(data, table_id);
      }

   }

   function CartItemIncDec(data, table_id) {
      //  console.log("::->",data);
      $.ajax({
         url: "{{url('" + table_id + "/CartItemIncDec')}}",
         method: 'POST',
         type: "post",
         cache: false,
         data: data,
         dataType: 'JSON',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         beforeSend: function() {
            $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
         },
         success: function(response) {
            cartItemCAll(table_id);
         },
         error: function(response) {}
      });
   }

   $(document).on("click", ".addtocart", function() {

     var checkUser =  `{{isset(auth()->user()->id) ? auth()->user()->id :0}}`;
   //   alert(checkUser);
     if(checkUser==null || checkUser.length==0)
     {
      $("#exampleModalTab").modal('show');
      toastr.error("Plase Login and regiser");
      return false; 
     }
     else{

     id = $(this).data("id");
      table_id = $("#table_id").val();
      price = $(this).data("price");
      id = $(this).data("id");
      selected = $(this).data("seleted");
      var routes = `{{url('/')}}/` + table_id + "/add_tocart";
      $.ajax({
         url: routes,
         data: {
            "product_id": id,
            "table_id": table_id,
            "price": price,
            'id': id
            },
            method: 'POST',
            type: "post",
            cache: false,
            dataType: 'JSON',
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
               $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
            },
            success: function(response) {
               $("#spinner").removeClass("show");
               if (response.status == true) {
                  toastr.success(response.message);
                  cartItemCAll(table_id);
               }else if(response.type=='1') {
                  $("#exampleModalTab").modal('show');
                  toastr.error(response.message);
               }
               else{
                  toastr.error(response.message);
               }
            },
            error: function(response) {}
         });
      }

      });

   $(document).on("click", ".remove_cartItem", function(e) {
      var cart_id = $(this).data("cart_id");
      var cart_item_id = $(this).data("cart_item_id");
      var table_id = $("#table_id").val();
      var index = $(this).data('index');
      var classs = ".remove_cartItem_" + index;
      $(classs).hide();

      //   alert(cart_id);
      $.ajax({
         url: "{{url('" + table_id + "/remove_cartItem')}}",
         method: 'POST',
         cache: false,
         data: {
            "cart_item_id": cart_item_id,
            "table_id": table_id,
            "cart_id": cart_id
         },
         dataType: 'JSON',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         beforeSend: function() {
            $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
         },
         success: function(response) {
            $("#spinner").removeClass("show");
            if (response.status == true) {
               toastr.success(response.message);
               cartItemCAll(table_id);
            } else {
               toastr.error(response.message);

            }
         },
         error: function(response) {}
      });

   });

   toastr.options = {
      "closeButton": true,
      "progressBar": true
   }
   cartItemCAll($("#table_id").val());

   function cartItemCAll(table_id) {
      $.ajax({
         url: "{{url('" + table_id + "/cartItemList')}}",
         method: 'POST',
         type: "post",
         cache: false,
         data: {
            "table_id": table_id,
         },
         dataType: 'JSON',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         beforeSend: function() {
            $("#spinner").addClass("show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center");
         },
         success: function(response) {
            $("#spinner").removeClass("show");
            html = "";
            if (response.status == true) {
               var result = response.data;

               var price = response.cart.price;
               var final_amount = response.cart.final_amount;
               var discount_amount = response.cart.discount_amount;
               var shipping_price = response.cart.shipping_price;
               var price = response.cart.price;
               var gstAmount = response.cart.gstAmount;

               $("#sub_total").text("Rs." + price);
               $("#discount_amount").text("Rs." + discount_amount);
               $("#shipping_amount").text("Rs." + shipping_price);
               $("#gstAmount").text("Rs." + gstAmount);
               $("#total_final_amount").text("Rs." + final_amount);

               $("#id_hidden").val(response.cart.id);
               $("#total_hidden").val(final_amount);
               $("#total_hidden2").val(final_amount);
               $("#sub_total_hidden").val(price);
               
               $("#paymentForm_withtblid").data("amount",total_final_amount);
              
               $("#paymentForm_withtblid").data("name","Restaurent Name");
               $("#paymentForm_withtblid").data("description","description Restaurent Name");
             
               
               $.each(result, function(key, value) {
                  var product_name = value.product_name;
                  var product_price = value.product_price;
                  var product_image = value.product_image;
                  var cart_id = value.cart_id;
                  var id = value.id;
                  var product_qty = value.qty;
                  html += '<div style="display: flex;" class="card mt-3 px-2 py-2 remove_cartItem_' + key + '">';
                  html += '<div style="display: flex;">';
                  html += '<img src="' + product_image + '" width="80vh" height="80vh" alt="">';
                  html += '<div>';
                  html += '<i class="bi bi-file-x remove_cartItem" data-index=' + key + ' data-cart_id=' + cart_id + ' data-cart_item_id=' + id + ' style="color: #fb0606; margin-left: 170px;"></i>';
                  html += '</div>';
                  html += '</div>';
                  html += '<div class="mx-2">';
                  html += '<p>' + product_name + '</p>';
                  html += ' <div class="quantity d-flex">';
                  html += '  <button onclick="increment(' + key + ',' + product_qty + ');return false;"    style="border: none; height: 37px; background-color: orange; color: white;">+</button>';
                  html += ' <input type="text" id="quantity" readonly value="' + product_qty + '" data-cart_id=' + cart_id + ' data-cart_item_id=' + id + ' class="quantity_' + key + '" style="background-color: orange; width: 60px; color: white;">';
                  html += '  <button onclick="decrement(' + key + ',' + product_qty + ');return false"  style="border: none; height: 37px; background-color: orange; color: white;">-</button>';
                  html += '  </div>';
                  html += '  <div>';
                  html += '    <p>Price : Rs.' + product_price + '</p>';
                  html += '    </div>';
                  html += '  </div>';
                  html += ' </div>';
               });
               $("#cartitemDiv").empty();
               $("#cartitemDiv").append(html);

            }
            // console.log();  
         },
         error: function(response) {}
      });
   }
</script>

<script>
   menuSelect($("#menu_id").val(), "{{auth()->user()->table_id ?? $restaurentName}}");

   function menuSelect(menu_id, tab_id) {
      formData = {
         menu_id: menu_id,
         tab_id: tab_id
      };
      route = "{{url('/')}}" + "/" + tab_id + "/getsub_menu_by_menu_id";
      ajaxCall(route, formData, 'sub_menu_div')
   }
</script>
@include('website.layout.footer')