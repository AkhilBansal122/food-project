@include('admin.layout.header')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



<script type="text/javascript" src='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js'></script>
<link media="screen" rel="stylesheet" href='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css' />
<link media="screen" rel="stylesheet" href='https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.css' />

<div class="page-wrapper">
    <div class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            @include('flash-message')

            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Ai Generative</li>
                    </ol>
                </nav>
            </div>
        </div>

        <!--end breadcrumb-->
        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card ">
                    <div class="card-body p-3">
                        @include('flash-message')
                        <form class="row" id="generativeForm" action="#" method="POST" enctype="multipart/form-data">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="title" id="title" required="true" aria-required="true" autocomplete="off" class="form-control" placholder="Please Enter search" placeholder="Please enter name" required id="title">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" id="submitbtn" class="btn btn-light px-5"> Send</button>
                                <button id="loadingbtn" class="btn btn-light px-5" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body" id="response">

                    </div>
                </div>
            </div>
        </div>
        <!--end row-->

    </div>
</div>

@include('admin.layout.footer')
<script>
    jQuery(document).ready(function($) {
        $("#loadingbtn").hide();
        $("#submitbtn").click(function(e) {
            // console.log("s");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                }
            });
            e.preventDefault();
            if (jQuery('#title').val() == '') {
                $("#title").attr("placeholder", "Please Enter Search ");
                return false;
            } else {


                var formData = {
                    search: jQuery('#title').val(),
                };
                var type = "POST";
                var ajaxurl = '{{route("open-ai")}}';
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    data: formData,
                    dataType: 'html',
                    beforeSend: function(msg) {

                        $("#submitbtn").prop('disabled', true);
                        $("#submitbtn").hide();
                        $("#loadingbtn").show();
                    },

                    success: function(data) {
                        $("#submitbtn").prop('disabled', false);
                        $("#submitbtn").show();
                        $("#loadingbtn").hide();
                        $("#response").html(data);
                        jQuery('#generativeForm').trigger("reset");
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
        });

    });
</script>