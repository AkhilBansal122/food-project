<?php
 $directoryURI = $_SERVER['REQUEST_URI'];
 $path = parse_url($directoryURI, PHP_URL_PATH);
 
 $components = explode('/', $path);
 
 $uri1= isset($components[2]) ? $components[2] :'';
 
 $uri2 = isset($components[3]) ? $components[3] :'';
 $checked = false;
 $logo= null;
 if(substr($uri1, 0, 4)==="TBL-")
 {
     $gettables =  App\Models\Tables::where("unique_id",$uri1)->first()->pluck('restaurent_id');
     
     if(!empty($gettables)){
         $getUser =  App\Models\User::where("id",$gettables[0])->where("is_admin",2)->first();
       //  dd($getUser); 
    $logo = public_path()."uploads/" .$getUser->logo ?? '';
   }
    $checked = true;    
    }else{
   $getUser =  App\Models\User::where("name",$uri1)->where("is_admin",2)->first();
   $logo = public_path()."uploads/" .$getUser->logo ?? '';
   $restaurentName = $uri1;
}
$about = App\Models\About::where("user_id",$getUser->id)->first();
?>
<div class="container-xxl py-5">
            <div class="container">
                <div class="row g-5 align-items-center">
                    <div class="col-lg-6">
                        <div class="row g-3">
                            <?php 
                            if(!empty($about)){
                                foreach(explode(",", $about->image) as $r){
                                    ?>
                                     <div class="col-6 text-start">
                                <img class="img-fluid rounded w-100 wow zoomIn" data-wow-delay="0.1s" src=" {{asset('public/')}}{{$r}}">
                            </div>
                                    <?php
                                }                                
                            }
                            else{
                                ?>
                                <div class="col-6 text-start">
                                <img class="img-fluid rounded w-100 wow zoomIn" data-wow-delay="0.1s" src=" {{asset('public/assets/website/img/about-1.jpg')}}">
                            </div>
                                <div class="col-6 text-start">
                                <img class="img-fluid rounded w-75 wow zoomIn" data-wow-delay="0.3s" src="{{asset('public/assets/website/img/about-2.jpg')}}" style="margin-top: 25%;">
                            </div>
                            <div class="col-6 text-end">
                                <img class="img-fluid rounded w-75 wow zoomIn" data-wow-delay="0.5s" src="{{asset('public/assets/website/img/about-3.jpg')}}">
                            </div>
                            <div class="col-6 text-end">
                                <img class="img-fluid rounded w-100 wow zoomIn" data-wow-delay="0.7s" src="{{asset('public/assets/website/img/about-4.jpg')}}">
                            </div>
                            <?php
                            }
                            ?>

                                                       
                            
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h5 class="section-title ff-secondary text-start text-primary fw-normal">About Us</h5>
                        <h1 class="mb-4">{{$about->title ?? 'welcome'}} <i class="fa fa-utensils text-primary me-2"></i>Restoran</h1>
                        <p class="mb-4">{{$about->description ?? '-'}}</p>
                    </div>
                </div>
            </div>
        </div>