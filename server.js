const express = require('express');
const app = express();
const cors = require('cors');
const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer,{
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    transports: ['websocket', 'polling'],
    credentials: true
},
allowEIO3: true
});

app.use(cors({
  origin: 'http://localhost:8000', // Update with your frontend origin
  credentials: true // Allow credentials in the request
}));

io.on('connection', (socket) => {
  console.log("connection");

  socket.on("disconnect", () => {
    console.log("disconnect");
  });

  socket.broadcast.emit('hi');
});

httpServer.listen(3000, () => {
  console.log("Server listening on port 3000");
});