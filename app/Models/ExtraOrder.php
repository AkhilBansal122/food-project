<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraOrder extends Model
{
    use HasFactory;
    protected $table ="extra_orders";

    public function customerDetails(){
        return $this->belongsTo(Customer::class,'customers_id');  
    }
     
    public function categoryDetails(){
        return $this->belongsTo(Category::class,'category_id');  
    }
    public function subDetails(){
        return $this->belongsTo(SubCategories::class,'sub_category_id');  
    }
}
