<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transation extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function orderDetails(){
        return $this->belongsTo(Orders::class,'order_id');  
    }

    public function customerDetails(){
        return $this->belongsTo(User::class,'from_id');  
    }
}
