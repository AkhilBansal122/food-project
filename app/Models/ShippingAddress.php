<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    use HasFactory;
    protected  $table = "shipping_addresses";

    public function orderDetails(){
        return $this->belongsTo(Orders::class,'order_id');  
    }
    public function userDetails(){
        return $this->belongsTo(User::class,'user_id');  
    }
    public function branchDetails(){
        return $this->belongsTo(Branch::class,'branch_id');  
    }
    public function customerDetails(){
        return $this->belongsTo(User::class,'customer_id');  
    }
    public function orderitemDetails(){
        return $this->belongsTo(OrdersItem::class,'order_id');  
    }
    
}

