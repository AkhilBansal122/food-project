<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $table ="customers";

    public function restaurentDetails(){
        return $this->belongsTo(User::class,'restaurent_id');  
    }
    public function managerDetails(){
        return $this->belongsTo(User::class,'manager_id');  
    }
    public function branchDetails(){
        return $this->belongsTo(Branch::class,'branch_id');  
    }

}
