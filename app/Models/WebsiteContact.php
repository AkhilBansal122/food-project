<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebsiteContact extends Model
{
    use HasFactory;
    protected $table = 'website_contact';
    protected $fillable = ['name', 'email', 'password', 'resturent_name'];
}
