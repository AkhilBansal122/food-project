<?php 
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\JsonResponse;
use Helper;
use App\Models\Category;
use App\Models\User;
use App\Models\Customer;
use App\Models\ExtraOrder;

class ExtraController extends Controller
{
    public function index()
    {
        if(auth()->user()->is_admin==3)
        {
            $category = Category::where(['user_id'=>auth()->user()->user_id,'status'=>'Active'] )->get();
           $customer =Customer::where("id",4)->first();
          //  dd($customer);
            $this->calaculate('',$customer);
            return view('manager/extraorder/index',compact('category'));
        }
    }
    public function extra_order_requestdata(Request $request)
    {

        if ($request->ajax()) {
            $user = auth()->user();
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search'];
            $unique_id = $request['unique_id'];
            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];

            $querydata = Customer::where("id", "!=", "0");
            if (auth()->user()->is_admin == 3) {
                $querydata->where('manager_id', '=', $user->id);
            }
            $totaldata = $querydata->count();
            $response = $querydata->offset($start)
                ->limit($limit)
                ->get();
            if (!$response) {
                $data = [];
            } else {
                $data = $response;
            }
            $datas = array();
            $i = 1;

            foreach ($data as $value) {
                $id = $value->id;
                $row['id'] = $i;
                $row['name'] = $value->name ?? "-";
                $row['mobile'] = $value->mobile ?? '-';
                $row['total_amount'] = isset($value->total_amount) ? $value->total_amount : '-';
                $row['discount_amount'] = isset($value->discount_amount) ? $value->discount_amount : '-';
                $row['final_amount'] = isset($value->final_amount) ? $value->final_amount : '-';
                $row['gst_amount'] = isset($value->gst_amount) ? $value->gst_amount : '-';
                $row['payment_mode'] = $value->payment_mode ?? '-';
                $row['status'] = $value->payment_status;
                
                $sel = "<select class='form-control select_changes3' data-id = '".$id."' data-payment_status = '".$value->payment_status."' >";

                if ($value->payment_status == 'Pending') {
                    $sel .= "<option value='Pending' " . ((isset($value->payment_status) && $value->payment_status == "Pending") ? 'Selected' : '') . ">Pending</option>";
                    $sel .= "<option value='Completed' " . ((isset($value->payment_status) && $value->payment_status == "Completed") ? 'Selected' : '') . ">Completed</option>";
                }
                if ($value->payment_status == 'Completed') {
                    $sel .= "<option value='Completed'" . ((isset($value->payment_status) && $value->payment_status == "Completed") ? 'Selected' : '') . ">Completed</option>";
                }
                $sel .= "</select>";
                $row['status'] = $sel;

                $view = '<a href="javascript:void(0);" data-id="' . $id . '" class="view_extra_order btn btn-light  radius-0 shadow btn-xs sharp me-1"><i class="bx bx-show"></i></a>';
                $row['actions'] = Helper::action($view);

                $datas[] = $row;
                $i++;
            }
            // dd($datas);
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas
            ];
            return response()->json($return);
        }
    }

    public function store(Request $request){
        if(auth()->user()->is_admin==3)
        {
            $validated = $request->validate([
                'name' => 'required',
                'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|numeric|digits:10'
                ]);
                $getCustomer = Customer::where(['mobile'=>$request->mobile,'payment_status'=>"Pending"])->first();
                $Customer = new Customer;
                $customerId = null;
                if(empty($getCustomer)){
                
                    $Customer ->branch_id =auth()->user()->branch_id;
                    $Customer ->manager_id= auth()->user()->id;
                    $Customer ->restaurent_id = auth()->user()->user_id;
                    $Customer ->name = $request->name;   
                    $Customer->mobile = $request->mobile;
                    $Customer->Save();
                    $customerId = $Customer->id;
                }
                else{
                    $customerId = $getCustomer->id;
                }
            //    dd($getCustomer);
                $ExtraOrder = new ExtraOrder;
                $ExtraOrder->customers_id=$customerId;
                $ExtraOrder->category_id = $request->category_id;
                $ExtraOrder->sub_category_id = $request->sub_category_id;
                $ExtraOrder->qty = $request->qty;
                $ExtraOrder->price = $request->price;
                $ExtraOrder->total_price = $request->total_price;
                $ExtraOrder->save();
                
                $ExtraOrder->unique_id = "EXTODR000".$ExtraOrder->id;
                
                $ExtraOrder->save();
                $Customer = $getCustomer ?? $Customer;
                $this->calaculate($ExtraOrder->id,$Customer);
                return back()->with('success','Order Added Successfully');
        }
    }

    public function calaculate($ExtraOrderId,$CustomerData){
        $getExtraOrder = ExtraOrder::where("customers_id",$CustomerData->id)->sum('total_price');
        $Customer = $CustomerData;
        $user =  User::select('dis_amount','dis_percent','bill_gst','GST')->where("id",$CustomerData->restaurent_id)->first();
        //dd($user);
       
        $dis_amount = $user->dis_amount ?? 0;
        $dis_percent = $user->dis_percent ?? 0;
        $bill_gst = $user->bill_gst;
        $GST = $user->GST;
        
        $final_amount = 0;
        $discount_amount = 0;
        $total_price = 0;
        $total_price =$getExtraOrder;
        
        if($total_price>=$dis_amount)
        {
            $discount_amount = $total_price * $dis_percent / 100;
        }
        else{
            $discount_amount = 0;
        }
        $Customer->discount_amount = $discount_amount;
        $final_amount = $total_price - $discount_amount;
        
        if ($bill_gst == 1) {
            $gstAmount = $final_amount * $GST / 100;
            
            $totalPrice = $final_amount + $gstAmount;
            $Customer->gst_amount = $gstAmount;
            
            $Customer->final_amount = $totalPrice;
            
        } else {
            $total_price = $final_amount;
            
            $Customer->final_amount = $total_price;
            
        }
        $Customer->total_amount = $getExtraOrder;
            $Customer->save();
            return true;
    }

    public function view_extra_order(Request $request){
        if(auth()->user()->is_admin==3)
        {
           $data= ExtraOrder::where("customers_id",$request->customers_id)->with('customerDetails.branchDetails','customerDetails','categoryDetails','subDetails','customerDetails.restaurentDetails')->get();
          // dd($data[0]['customerDetails']['restaurentDetails']);
           return view('manager/extraorder/Invoice',compact('data'));
        }
    }

    public function extra_status_change(Request $request){
        if(auth()->user()->is_admin==3)
        {
            if($request->payment_status=='Pending'){
                Customer::where("id",$request->customers_id)->update(['payment_status'=>"Completed"]);
               $update= ExtraOrder::where("customers_id",$request->customers_id)->update(['order_status'=>"Completed"]);
                if ($update) {
                    return response()->json(['status' => true, "message" => "Payment Successfully"]);
                } else {
                    return response()->json(['status' => false, "message" => "Payment Failed"]);
                }

            }
        }

    }
}
