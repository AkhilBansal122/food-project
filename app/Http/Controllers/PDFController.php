<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use PDF;
use App\Models\ShippingAddress;
use App\Models\OrdersItem;  
class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF()
    {
        $order_id = 19;
           $getShipping = ShippingAddress::where("order_id",$order_id)
            ->with(['orderDetails','userDetails','branchDetails','customerDetails','orderitemDetails'])
            ->first();
            $OrdersItem = OrdersItem::where("order_id",$order_id)->get();
            $getShipping->order_item = $OrdersItem ?? null;
         
        $data = [
            'getShipping' =>$getShipping 
        ];
   //     dd($data);          
        $pdf = PDF::loadView('myPDF', $data);
        
      //  return $pdf->download('itsolutionstuff.pdf');
    }
}
