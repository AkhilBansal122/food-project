<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

use App\Models\Cart;
use App\Models\cartItem;
use App\Models\OrdersItem;
use App\Models\Tables;
use App\Models\Orders;
use App\Models\Transation;
use App\Models\CustomOrders;
use App\Models\User;
use App\Models\ShippingAddress;
use Session;
class PhonepeController extends Controller
{
    var $shippingData = [];
    
    public function Phonepe(Request $request)
    {
        
        $input = $request->all();
       // dd(auth()->user());
       // auth()->user()->shippingData = $input;
        //   $shippingData[] = $input;
        //  array_push($shippingData,$input);
        //  Session::put('paymentData', "test");
      //  $request->session()->put('paymentData', $input);
      $input['is_admin']=auth()->user()->is_admin;
      $input['user_id']=auth()->user()->id;
      $input['branch_id']=auth()->user()->branch_id;
      $input['order_mode']=1;
     
        
        $data = array(
            'merchantId' => 'MERCHANTUAT',
            'merchantTransactionId' => 'MT7850590068188103',
            'merchantUserId' => 'MUID123',
            'amount' =>$input['amount']*100,
            'redirectUrl' => route('response',['data'=>$input]),
            'redirectMode' => 'POST',
            'callbackUrl' => route('response',['data'=>$input]),
            'mobileNumber' => $input['phone'] ?? auth()->user()->mobile_number,
            'paymentInstrument' =>
            array(
                'type' => 'PAY_PAGE',
            ),
        );

        $encode = base64_encode(json_encode($data));

        $saltKey = '099eb0cd-02cf-4e2a-8aca-3e6c6aff0399';
        $saltIndex = 1;

        $string = $encode . '/pg/v1/pay' . $saltKey;
        $sha256 = hash('sha256', $string);

        $finalXHeader = $sha256 . '###' . $saltIndex;
          $response = Curl::to('https://api-preprod.phonepe.com/apis/merchant-simulator/pg/v1/pay')
            ->withHeader('Content-Type:application/json')
            ->withHeader('X-VERIFY:' . $finalXHeader)
            ->withData(json_encode(['request' => $encode]))
            ->post();

            $passData = json_encode(['response'=>$encode]);
            $method="GET";
            $header=[
                'Content-Type'=>'application/json',
                'X-VERIFY'=>$finalXHeader,
            ];
            $rData = json_decode($response);
   //       dd($rData);
        return redirect()->to($rData->data->instrumentResponse->redirectInfo->url);
    }
    
    public function response(Request $request)
    {
        $input = $request->all();
        $shippingData = $input['data'];
      //  dd($input);

        $saltKey = '099eb0cd-02cf-4e2a-8aca-3e6c6aff0399';
        $saltIndex = 1;

        $finalXHeader = hash('sha256','/pg/v1/status/'.$input['merchantId'].'/'.$input['transactionId'].$saltKey).'###'.$saltIndex;

        $response = Curl::to('https://api-preprod.phonepe.com/apis/merchant-simulator/pg/v1/status/'.$input['merchantId'].'/'.$input['transactionId'])
                ->withHeader('Content-Type:application/json')
                ->withHeader('accept:application/json')
                ->withHeader('X-VERIFY:'.$finalXHeader)
                ->withHeader('X-MERCHANT-ID:'.$input['transactionId'])
                ->get();

                if(($shippingData) && $shippingData['is_admin']==5){
                    $id =$shippingData['table_id']; //$request->table_id;
                    $user_id = $shippingData['user_id'];
                    $get_cart = Cart::where("user_id",$user_id)->first();
                    $table= Tables::where("unique_id",$id)->first();
                    $branch_id  =$table->get_manager->branch_id ?? $shippingData['branch_id'];
                    //dd($branch_id);
                    $datashipping = $shippingData ?? null;
                  // dd($datashipping);
                    $order  =new Orders();
                    if(!is_null($get_cart)){
                        $get_cartItem = cartItem::where("cart_id",$get_cart->id)->get();
        
                        $order->table_id =$id; //$request->table_id;
                        $order->user_id = $get_cart->user_id;
                        $order->coupon_id = $get_cart->coupon_id;
                        $order->coupon_code = $get_cart->coupon_code;
                        $order->price = $get_cart->price;
                        $order->discount_amount = $get_cart->discount_amount;
                        $order->final_amount = $get_cart->final_amount;
                        $order->shipping_price = $get_cart->shipping_price;
                        $order->order_in_process = 0;
                        $order->branch_id= $branch_id;
                        $order->save();
                        $order->unique_id = "ODR-0000".$order->id;
                        $order->order_mode = $shippingData['order_mode'] ?? 0;
                        $order->save();
                        if(!empty($get_cartItem)){
                            foreach($get_cartItem as $value){
                                $orderItem = new OrdersItem();
                                $orderItem->order_id = $order->id;
                                $orderItem->user_id = $user_id;
                                $orderItem->product_id = $value->product_id;
                                $orderItem->qty = $value->qty;
                                $orderItem->product_price = $value->product_price;
                                $orderItem->save();
                            }
                        }
                        $getRestaurent= User::where('name','Like','%'.$request->table_id.'%')->first();
                          
                        if(!is_null($datashipping) &&!empty($datashipping['first_name']))
                        {
                            $ShippingAddress = new ShippingAddress();
                            $ShippingAddress->order_id = $order->id;
                            $ShippingAddress->user_id = $getRestaurent->id ?? 0;
                            $ShippingAddress->customer_id = $user_id;
                            $ShippingAddress->branch_id = $datashipping['branch_id'];
                            $ShippingAddress->first_name = $datashipping['first_name'];
                            $ShippingAddress->last_name = $datashipping['last_name'];
                            $ShippingAddress->email = $datashipping['email'];
                            $ShippingAddress->phone = $datashipping['phone'];
                            $ShippingAddress->country= $datashipping['country'];
                            $ShippingAddress->address = $datashipping['address_one'];
                            $ShippingAddress->city = $datashipping['city'];
                            $ShippingAddress->state = $datashipping['state'];
                            $ShippingAddress->postcode = $datashipping['zip'];
                            $ShippingAddress->save();
                        }
                        $Transation = new Transation();
                        $Transation->order_id = $order->id;
                        $Transation->from_id  =$user_id;
                        $Transation->to_id =$getRestaurent->id ?? $table->restaurent_id;
                        $Transation->status='Successfully';
                        $Transation->save();
                        $order->transation_id = $Transation->id;
                        $order->save();
                        $Transation->unique_id = "TRN-0000".$Transation->id;
                        $Transation->payment_id =$input['transactionId']; //$request->razorpay_payment_id;
                        $Transation->save();
                        cartItem::where("cart_id",$get_cart->id)->delete();
                        Cart::where("id",$get_cart->id)->delete();
                       $redirecturl ="/".$id;
                        return redirect($redirecturl)->with('success','Payment successfully');
                        // return response()->json(['type'=>true,'success' => 'Payment successfully']);
                    }
                }    
//        dd(json_decode($response));
    }
   
}
