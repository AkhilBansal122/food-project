<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\User;
use Stripe;
use Session;
use Exception;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('subscription.create');
    }
	
	public function cancel(){
	     $user=auth()->user();
            $subscriptions = $user->subscriptions()->active()->first(); // getting all the active subscriptions 
		 $subscriptions->cancel();
	}
    public function orderPost(Request $request)
    {
     
			$user = auth()->user();
            $input = $request->all();
			$token =  $request->stripeToken;
            $paymentMethod = $request->paymentMethod;
            try {

                Stripe\Stripe::setApiKey("sk_test_51KdbubSAaG2Fx1sH8MxJpmlcUPTJpCynJvg251fOkYJX7ShavAYSIoJsaCE2rmF3aH6wJUPdinJTuxm9z2uKlO7H00coCnr3oI");
                
                if (is_null($user->stripe_id)) {
                    $stripeCustomer = $user->createAsStripeCustomer();
                }

             $source=   \Stripe\Customer::createSource(
                    $user->stripe_id,
                    ['source' => $token]
                );

                $user->newSubscription('test','price_1LcYNzSAaG2Fx1sHQBVYjxH5')
                    ->create($source->id, [
                    'email' => $user->email,
                ]);

                return back()->with('success','Subscription is completed.');
            } catch (Exception $e) {
				dd($e->getMessage());
                return back()->with('success',$e->getMessage());
            }
            
    }
}