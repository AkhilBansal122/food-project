/*

Give the service worker access to Firebase Messaging.

Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.

*/

importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');

importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');



/*

Initialize the Firebase app in the service worker by passing in the messagingSenderId.

* New configuration for app@pulseservice.com

*/

firebase.initializeApp({

    apiKey: "AIzaSyCCyrDtRnY4_bscEVL9mhKsCAMX2TPOzLs",

    authDomain: "image-project-3aaa8.firebaseapp.com",

    databaseURL: "https://foodproject-36798.firebaseio.com",

    projectId: "image-project-3aaa8",

    storageBucket: "image-project-3aaa8.appspot.com",

    messagingSenderId: "991819697981",

    appId: "1:991819697981:web:a963ca036a514e08a43f54",

    measurementId: "G-FT00FSXMTP"

});



/*

Retrieve an instance of Firebase Messaging so that it can handle background messages.

*/

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {

    console.log(

        "[firebase-messaging-sw.js] Received background message ",

        payload,

    );

    /* Customize notification here */

    const notificationTitle = "Background Message Title";

    const notificationOptions = {

        body: "Background Message body.",

        icon: "/itwonders-web-logo.png",

    };



    return self.registration.showNotification(

        notificationTitle,

        notificationOptions,

    );

});