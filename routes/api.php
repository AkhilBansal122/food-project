<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ApiController;

Route::get('/test', [ApiController::class, 'test'])->name('/api/test');

